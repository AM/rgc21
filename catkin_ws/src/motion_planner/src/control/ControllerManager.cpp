/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/control/ControllerManager.hpp"
#include <controller_manager_msgs/SwitchController.h>
#include <ros/ros.h>

//! Special Constructor.
Robothon::Planner::ControllerManager::ControllerManager(ros::NodeHandle& nh)
    : m_switchControllerClient{ nh.serviceClient<controller_manager_msgs::SwitchController>(
          "controller_manager/switch_controller") }
{
    // Wait for the controller_manager to start up and provide the required service.
    m_switchControllerClient.waitForExistence();
    // Get the custom controllers from the ROS Parameter Server.
    XmlRpc::XmlRpcValue objectList;
    ros::param::get("/robothon/custom_controllers", objectList);
    for (int ii = 0; ii < objectList.size(); ++ii) { // NOLINT
        m_customControllers.emplace_back(objectList[ii]);
    }
}

//! Activate, i.e. load a different controller.
bool Robothon::Planner::ControllerManager::switchController(const std::string& controllerName)
{
    // Get a container with all other controllers which have to be stopped.
    std::vector<std::string> otherControllers = m_customControllers;
    otherControllers.erase(
        std::remove(otherControllers.begin(), otherControllers.end(), controllerName), otherControllers.end());
    // Switch the controllers with best effort, i.e. do not trigger an error if a controller that is already stopped is
    // stopped again etc.
    controller_manager_msgs::SwitchController switchCall;
    switchCall.request.stop_controllers = otherControllers;
    switchCall.request.start_controllers.emplace_back(controllerName);
    switchCall.request.strictness = controller_manager_msgs::SwitchController::Request::BEST_EFFORT;
    if (!m_switchControllerClient.call(switchCall)) {
        ROS_ERROR("ControllerManager.cpp: Unable to access controller manager to switch controllers!");
    }
    return switchCall.response.ok;
}
