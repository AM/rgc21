// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
// Source code was taken from: https://github.com/frankaemika/icra18-fci-tutorial/tree/master/icra18/src
// Source code was modified by Wittmann 2021: see comments on changes in the detailed class description.

/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/control/controllers/ParameterizableCompliantAssemblyController.hpp"
#include <cmath>
#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <franka_hw/franka_model_interface.h>
#include <franka_hw/franka_state_interface.h>
#include <memory>
#include <pluginlib/class_list_macros.h>

//! Mandatory initialization function that is called when the controller is loaded.
bool Robothon::Planner::ParameterizableCompliantAssemblyController::init(
    hardware_interface::RobotHW* robot, ros::NodeHandle& nodeHandle)
{
    // Initialize logger.
    m_logger = std::make_unique<Logger>("robothon_log/pcac.txt");
    m_logger->openLogfile(
        "Time;dForceError0;dForceError1;dForceError2;forceError0;forceError1;forceError2;iniExtFX;iniExtFY;"
        "iniExtFZ;iniExtMX;iniExtMY;iniExtMZ;impErr0;impErr1;impErr2;impErr3;impErr4;impErr5;filtForceErr0;"
        "filtForceErr1;filtForceErr2"
        ";filtForceErr3;filtForceErr4;filtForceErr5");

    // Initialize filter.
    m_secOrderLowPassForceError
        = std::make_unique<SecOrderLowPass<Eigen::Matrix<double, 6, 1>>>(Eigen::Matrix<double, 6, 1>::Zero());
    m_secOrderLowPassForceError->init(0.001, 0.4, 0.9);

    // Get arm name and joint names from the ROS parameter server.
    std::vector<std::string> jointNames;
    std::string robotName;
    if (!nodeHandle.getParam("arm_id", robotName)) {
        ROS_ERROR("ParameterizableCompliantAssemblyController: Could not read parameter arm_id");
        return false;
    }
    if (!nodeHandle.getParam("joint_names", jointNames) || jointNames.size() != 7) {
        ROS_ERROR("ParameterizableCompliantAssemblyController: Invalid or no joint_names parameters provided, aborting "
                  "controller init!");
        return false;
    }
    // Get the three hardware interfaces that the controller claims (robot model, robot state and torque interface).
    auto* modelInterface = robot->get<franka_hw::FrankaModelInterface>();
    if (modelInterface == nullptr) {
        ROS_ERROR_STREAM("ParameterizableCompliantAssemblyController: Error getting model interface from hardware!");
        return false;
    }
    try {
        m_robotModelHandle
            = std::make_unique<franka_hw::FrankaModelHandle>(modelInterface->getHandle(robotName + "_model"));
    } catch (hardware_interface::HardwareInterfaceException& ex) {
        ROS_ERROR_STREAM(
            "ParameterizableCompliantAssemblyController: Exception getting model handle from interface: " << ex.what());
        return false;
    }
    auto* stateInterface = robot->get<franka_hw::FrankaStateInterface>();
    if (stateInterface == nullptr) {
        ROS_ERROR_STREAM("ParameterizableCompliantAssemblyController: Error getting state interface from hardware!");
        return false;
    }
    try {
        m_robotStateHandle
            = std::make_unique<franka_hw::FrankaStateHandle>(stateInterface->getHandle(robotName + "_robot"));
    } catch (hardware_interface::HardwareInterfaceException& ex) {
        ROS_ERROR_STREAM(
            "ParameterizableCompliantAssemblyController: Exception getting state handle from interface: " << ex.what());
        return false;
    }
    auto* effortJointInterface = robot->get<hardware_interface::EffortJointInterface>();
    if (effortJointInterface == nullptr) {
        ROS_ERROR_STREAM(
            "ParameterizableCompliantAssemblyController: Error getting effort joint interface from hardware!");
        return false;
    }
    for (size_t ii = 0; ii < 7; ++ii) {
        try {
            m_jointHandles.push_back(effortJointInterface->getHandle(jointNames[ii]));
        } catch (const hardware_interface::HardwareInterfaceException& ex) {
            ROS_ERROR_STREAM(
                "ParameterizableCompliantAssemblyController: Exception getting joint handles: " << ex.what());
            return false;
        }
    }
    return true;
}

//! Optional function that gets executed before the first update.
void Robothon::Planner::ParameterizableCompliantAssemblyController::starting(const ros::Time& /*time*/)
{
    // Get current sensor values.
    franka::RobotState currentRobotState = m_robotStateHandle->getRobotState();
    // Get the initial values.
    Eigen::Map<Eigen::Matrix<double, 6, 1>> extForceOK(currentRobotState.O_F_ext_hat_K.data());
    Eigen::Map<Eigen::Matrix<double, 6, 1>> extForceKK(currentRobotState.K_F_ext_hat_K.data());
    m_initialExternalForce << extForceOK[0], extForceOK[1], extForceOK[2], extForceKK[3], extForceKK[4], extForceKK[5];
    Eigen::Affine3d initialTransform(Eigen::Matrix4d::Map(currentRobotState.O_T_EE.data()));
    m_initialPosition = initialTransform.translation();
    m_initialOrientation = Eigen::Quaterniond(initialTransform.linear());
    m_currentTime = 0.0;
    // Reset values.
    m_forceError.setZero();
    m_dForceError.setZero();
    m_filteredForceError.setZero();
    m_intForceError.setZero();
    m_impedanceError.setZero();
    m_intImpedanceError.setZero();
    // Read the controller configuration from the ROS parameter server.
    std::vector<double> kp;
    if (!ros::param::get("/robothon/online/Kp_imp", kp)) {
        ROS_ERROR("Could not read Kp_imp from the ROS parameter server.");
    }
    for (int ii = 0; ii < 6; ++ii) {
        m_Kp_imp.diagonal()[ii] = kp.at(ii);
        m_Kd_imp.diagonal()[ii] = 2.0 * std::sqrt(m_Kp_imp.diagonal()[ii]);
        m_Ki_imp.diagonal()[ii] = 0.0; // Currently hardcoded. ToDo!
    }
    if (!ros::param::get("/robothon/online/Kp_force", m_Kp_force)) {
        ROS_ERROR("Could not read Kp_force from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/online/Ki_force", m_Ki_force)) {
        ROS_ERROR("Could not read Ki_force from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/online/Kd_force", m_Kd_force)) {
        ROS_ERROR("Could not read Kd_force from the ROS parameter server.");
    }
    std::vector<double> fdes;
    if (!ros::param::get("/robothon/online/desiredForce", fdes)) {
        ROS_ERROR("Could not read desiredForce from the ROS parameter server.");
    }
    for (int ii = 0; ii < 6; ++ii) {
        m_desiredForce[ii] = fdes.at(ii);
    }
    if (!ros::param::get("/robothon/online/wiggleFrequencyX", m_wiggleFrequencyX)) {
        ROS_ERROR("Could not read wiggleFrequencyX from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/online/wiggleFrequencyY", m_wiggleFrequencyY)) {
        ROS_ERROR("Could not read wiggleFrequencyY from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/online/wiggleAmplitudeX", m_wiggleAmplitudeX)) {
        ROS_ERROR("Could not read wiggleAmplitudeX from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/online/wiggleAmplitudeY", m_wiggleAmplitudeY)) {
        ROS_ERROR("Could not read wiggleAmplitudeY from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/online/useWiggle", m_useWiggle)) {
        ROS_ERROR("Could not read the useWiggle from the ROS parameter server.");
    }
    m_secOrderLowPassForceError->reset(m_desiredForce);
}

//! Mandatory update function that gets called within each control cycle.
void Robothon::Planner::ParameterizableCompliantAssemblyController::update(
    const ros::Time& /*time*/, const ros::Duration& period)
{
    m_currentTime += period.toSec();
    // Get the current sensor and model data.
    franka::RobotState currentRobotState = m_robotStateHandle->getRobotState();
    std::array<double, 42> jacobianArrayBase = m_robotModelHandle->getZeroJacobian(franka::Frame::kEndEffector);
    std::array<double, 42> jacobianArrayEE = m_robotModelHandle->getBodyJacobian(franka::Frame::kEndEffector);
    std::array<double, 7> coriolisArray = m_robotModelHandle->getCoriolis();
    Eigen::Map<Eigen::Matrix<double, 7, 1>> coriolis(coriolisArray.data());
    Eigen::Map<Eigen::Matrix<double, 6, 7>> jacobianBase(jacobianArrayBase.data());
    Eigen::Map<Eigen::Matrix<double, 6, 7>> jacobianEE(jacobianArrayEE.data());
    Eigen::Map<Eigen::Matrix<double, 6, 1>> extForceOK(currentRobotState.O_F_ext_hat_K.data());
    Eigen::Map<Eigen::Matrix<double, 6, 1>> extForceKK(currentRobotState.K_F_ext_hat_K.data());
    Eigen::Map<Eigen::Matrix<double, 7, 1>> dq(currentRobotState.dq.data());
    Eigen::Affine3d transform(Eigen::Matrix4d::Map(currentRobotState.O_T_EE.data()));
    Eigen::Vector3d position(transform.translation());
    Eigen::Quaterniond orientation(transform.linear());
    // PID control of the desired force. Translational part expressed w.r.t. the base frame, rotational part expressed
    // w.r.t. the end effector fame.
    Eigen::Matrix<double, 6, 1> extForce;
    extForce << extForceOK[0], extForceOK[1], extForceOK[2], extForceKK[3], extForceKK[4], extForceKK[5];
    // Filter the values of the force error and compute the derivative part.
    m_forceError = m_desiredForce - extForce; // + m_initialExternalForce;
    m_secOrderLowPassForceError->process(m_forceError);
    m_filteredForceError = m_secOrderLowPassForceError->getOutput(); // Only for logging.
    m_dForceError = m_secOrderLowPassForceError->getDerivative(); // For SecondOrderLowPassFilter.
    m_intForceError = m_intForceError + period.toSec() * m_forceError;
    Eigen::Matrix<double, 6, 1> piPushControl
        = m_Kp_force * m_forceError + m_Kd_force * m_dForceError + m_Ki_force * m_intForceError;
    // Add translational and rotational error.
    Eigen::Matrix<double, 7, 1> piPushControlTorque = jacobianBase.block<3, 7>(0, 0).transpose() * piPushControl.head(3)
        + jacobianEE.block<3, 7>(3, 0).transpose() * piPushControl.tail(3);
    // Cartesian impedance PID control.
    m_impedanceError.head(3) << m_initialPosition - position;
    Eigen::Quaterniond currentOrientation = orientation;
    if (!m_useWiggle) {
        if (m_initialOrientation.coeffs().dot(orientation.coeffs()) < 0.0) {
            orientation.coeffs() << -orientation.coeffs();
        }
        Eigen::Quaterniond errorQuaternion(orientation.inverse() * m_initialOrientation);
        m_impedanceError.tail(3) << currentOrientation * errorQuaternion.vec();
    } else {
        // Wiggle the orientation of the end effector.
        Eigen::Quaterniond wiggledOrientation = wiggleOrientation();
        if (wiggledOrientation.coeffs().dot(orientation.coeffs()) < 0.0) {
            orientation.coeffs() << -orientation.coeffs();
        }
        Eigen::Quaterniond errorQuaternion(orientation.inverse() * wiggledOrientation);
        m_impedanceError.tail(3) << currentOrientation * errorQuaternion.vec();
    }
    m_intImpedanceError = m_intImpedanceError + period.toSec() * m_impedanceError;
    Eigen::Matrix<double, 6, 1> impedance
        = m_Kp_imp * m_impedanceError - m_Kd_imp * jacobianBase * dq + m_Ki_imp * m_intImpedanceError;
    Eigen::Matrix<double, 7, 1> impedanceTorque = jacobianBase.transpose() * impedance;

    // Command the torques to the robot with feed forward of coriolis effects.
    Eigen::Matrix<double, 7, 1> commandedTorque = coriolis + impedanceTorque + piPushControlTorque;
    for (size_t i = 0; i < 7; ++i) {
        m_jointHandles[i].setCommand(commandedTorque[i]);
    }
    try {
        m_logger->writeToLogfile(std::array<double, 24>({ m_dForceError[0], m_dForceError[1], m_dForceError[2],
            m_forceError[0], m_forceError[1], m_forceError[2], m_initialExternalForce[0], m_initialExternalForce[1],
            m_initialExternalForce[2], m_initialExternalForce[3], m_initialExternalForce[4], m_initialExternalForce[5],
            m_impedanceError[0], m_impedanceError[1], m_impedanceError[2], m_impedanceError[3], m_impedanceError[4],
            m_impedanceError[5], m_filteredForceError[0], m_filteredForceError[1], m_filteredForceError[2],
            m_filteredForceError[3], m_filteredForceError[4], m_filteredForceError[5] }));
    } catch (const std::exception& e) {
        std::cerr << e.what();
    }
}

//! Perform a wiggle motion of the end effector around its \ref m_prePlugInOrientation.
Eigen::Quaterniond Robothon::Planner::ParameterizableCompliantAssemblyController::wiggleOrientation() const
{
    Eigen::AngleAxisd angleAxisWiggleX;
    angleAxisWiggleX.axis() << 1, 0, 0;
    angleAxisWiggleX.angle() = sin(2.0 * M_PI * m_currentTime * m_wiggleFrequencyX) * m_wiggleAmplitudeX;
    Eigen::AngleAxisd angleAxisWiggleY;
    angleAxisWiggleY.axis() << 0, 1, 0;
    angleAxisWiggleY.angle() = sin(2.0 * M_PI * m_currentTime * m_wiggleFrequencyY) * m_wiggleAmplitudeY;
    Eigen::Quaterniond wiggleX(angleAxisWiggleX);
    Eigen::Quaterniond wiggleY(angleAxisWiggleY);
    return (wiggleY * (wiggleX * m_initialOrientation)).normalized();
}

// Export the controller as a plug in via the ROS pluginlib.
PLUGINLIB_EXPORT_CLASS(
    Robothon::Planner::ParameterizableCompliantAssemblyController, controller_interface::ControllerBase)
