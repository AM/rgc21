/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/MotionPlanner.hpp"
#include "motion_planner/utils/MotionPlannerConfig.hpp"
#include <ros/node_handle.h>

//! Spins the motion planner.
int main(int argc, char* argv[])
{
    // Initialize the ROS node.
    ros::init(argc, argv, "motion_planner_node");
    ros::NodeHandle nh;
    // Load the configuration parameters of the motion planner from the ROS Parameter server.
    Robothon::Planner::MotionPlannerConfig::get();
    // Start an second thread to get the current robot state.
    ros::AsyncSpinner spinner(2);
    spinner.start();
    // Initialize the motion planner instance with its ROS server.
    Robothon::Planner::MotionPlanner motionPlanner(nh);
    // Spin the node.
    ros::waitForShutdown();
    // ros::spin() will exit when Ctrl-C is pressed, or the node is shutdown by the master.
    return 0;
}
