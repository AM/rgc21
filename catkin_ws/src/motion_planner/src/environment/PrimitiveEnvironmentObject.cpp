/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/environment/PrimitiveEnvironmentObject.hpp"
#include <ros/ros.h>
#include <string>

//! Specialized constructor.
Robothon::Planner::PrimitiveEnvironmentObject::PrimitiveEnvironmentObject(StaticObjectType object)
{
    // Read the correct ROS parameters.
    switch (object) {
    case StaticObjectType::TABLE:
        m_name = "table";
        break;
    case StaticObjectType::TASK_BOARD:
        m_name = "task_board";
        break;
    default:
        ROS_ERROR("PrimitiveEnvironmentObject.cpp: Trying to load unknown static environment object!");
    }
    XmlRpc::XmlRpcValue objectList;
    ros::param::get("/robothon/" + m_name, objectList);
    ROS_INFO_STREAM(
        "PrimitiveEnvironmentObject.cpp: Loading " << objectList.size() << " collision objects for " << m_name << ".");

    // Generate the shapes forming the object and add an offset.
    double safetyDistance = 0.0;
    if (!ros::param::get("safetyDistance", safetyDistance)) {
        ROS_ERROR("PrimitiveEnvironmentObject.cpp: Failed to load the task_board_offset ROS parameter!");
    }
    for (int ii = 0; ii < objectList.size(); ++ii) { // NOLINT
        safetyDistance = (m_name == "task_board") ? safetyDistance : 0.0;
        m_primitiveShapes.emplace_back(PrimitiveShape(objectList[ii], safetyDistance));
    }
}

//! Generates corresponding ROS message that defines the shapes forming the object.
std::tuple<std::vector<shape_msgs::SolidPrimitive>, std::vector<geometry_msgs::Pose>>
Robothon::Planner::PrimitiveEnvironmentObject::generateRosMessage(
    const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation)
{
    // Get the objects pose w.r.t. the robot's base frame which will be used to transform the individual shapes.
    Eigen::Isometry3d objectInBaseFrame;
    objectInBaseFrame.setIdentity();
    objectInBaseFrame.translation() = position;
    objectInBaseFrame.linear() = orientation.toRotationMatrix();

    // Containers for solid primitives with corresponding poses.
    std::vector<shape_msgs::SolidPrimitive> primitives;
    std::vector<geometry_msgs::Pose> primitivePoses;

    // Transform the shapes to the global pose and get the ROS message information.
    for (const auto& element : m_primitiveShapes) {
        shape_msgs::SolidPrimitive primitive;
        switch (element.m_primitiveShapeType) {
        case PrimitiveShapeType::BOX:
            primitive.type = primitive.BOX;
            break;
        case PrimitiveShapeType::CYLINDER:
            primitive.type = primitive.CYLINDER;
            break;
        case PrimitiveShapeType::CONE:
            primitive.type = primitive.CONE;
            break;
        case PrimitiveShapeType::SPHERE:
            primitive.type = primitive.SPHERE;
            break;
        default:
            ROS_ERROR("PrimitiveEnvironmentObject.cpp: Trying to load unknown shape object!");
        }
        primitive.dimensions = element.m_dimensions;
        primitives.emplace_back(primitive);
        // Shape's pose in the object's frame.
        Eigen::Isometry3d shapeInObjectFrame;
        shapeInObjectFrame.setIdentity();
        shapeInObjectFrame.translation() = element.m_positionInObjectFrame;
        shapeInObjectFrame.linear() = element.m_orientationInObjectFrame.toRotationMatrix();
        // Shape's pose in the robot's base frame.
        Eigen::Isometry3d shapeInBaseFrame;
        shapeInBaseFrame.setIdentity();
        shapeInBaseFrame = objectInBaseFrame * shapeInObjectFrame;
        primitivePoses.emplace_back(
            getRosPose(shapeInBaseFrame.translation(), static_cast<Eigen::Quaterniond>(shapeInBaseFrame.linear())));
    }

    return { primitives, primitivePoses };
}
