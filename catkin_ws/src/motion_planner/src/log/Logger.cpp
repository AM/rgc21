/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/log/Logger.h"
#include <array>
#include <experimental/filesystem>
#include <iostream>
#include <utility>

//! Constructor.
Robothon::Planner::Logger::Logger(std::string fileName)
    : m_fileName{ std::move(fileName) }
    , m_startTime{ std::chrono::high_resolution_clock::now() }
{
}

//! Opens the logfile after the object was instantiated with the constructor above. A header is already printed.
void Robothon::Planner::Logger::openLogfile(const std::string& header)
{
    // Generate the directory for the log files, if it does not exist yet.
    std::experimental::filesystem::create_directory("robothon_log");
    m_logFileHandle.open(m_fileName);
    m_logFileHandle << header << "\n";
}
