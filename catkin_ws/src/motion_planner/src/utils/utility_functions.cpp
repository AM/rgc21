/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/utils/utility_functions.hpp"

//! Converts an Eigen pose to the corresponding ROS message type.
geometry_msgs::Pose Robothon::Planner::getRosPose(
    const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation)
{
    geometry_msgs::Pose pose;
    pose.position.x = position.x();
    pose.position.y = position.y();
    pose.position.z = position.z();
    pose.orientation.x = orientation.x();
    pose.orientation.y = orientation.y();
    pose.orientation.z = orientation.z();
    pose.orientation.w = orientation.w();
    return pose;
}

//! Converts an Eigen pose to the corresponding ROS message type.
geometry_msgs::Pose Robothon::Planner::getRosPose(const Eigen::Isometry3d& pose)
{
    auto [position, orientation] = mat2posRot(pose);
    return getRosPose(position, orientation);
}

//! Formulates a move to task space goal service based on a provided Eigen pose.
motion_planner::MoveTaskSpace Robothon::Planner::getMoveTaskSpaceService(
    const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation)
{
    motion_planner::MoveTaskSpace aboveBox;
    aboveBox.request.goal = getRosPose(position, orientation);
    return aboveBox;
}

//! Formulates a move to task space goal service based on a provided Eigen pose.
motion_planner::MoveTaskSpace Robothon::Planner::getMoveTaskSpaceService(const Eigen::Isometry3d& pose)
{
    motion_planner::MoveTaskSpace moveTaskSpace;
    moveTaskSpace.request.goal = getRosPose(pose);
    return moveTaskSpace;
}

//! Formulates a move to joint space goal service based on provided joint angles.
motion_planner::MoveJointSpace Robothon::Planner::getMoveJointSpaceService(const std::array<double, 7>& configuration)
{
    motion_planner::MoveJointSpace jointPositions;
    jointPositions.request.goal.resize(configuration.size());
    for (const auto* it = configuration.begin(); it != configuration.end(); ++it) {
        jointPositions.request.goal.at(it - configuration.begin()) = *it;
    }
    return std::move(jointPositions);
}

//! Converts a ROS pose to an Eigen position and orientation.
std::tuple<Eigen::Vector3d, Eigen::Quaterniond> Robothon::Planner::getEigenPose(geometry_msgs::Pose pose)
{
    return { (Eigen::Vector3d() << pose.position.x, pose.position.y, pose.position.z).finished(),
        Eigen::Quaterniond(pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z) };
}

//! Convert position and orientation to a homogeneous transformation matrix.
Eigen::Isometry3d Robothon::Planner::posRot2mat(const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation)
{
    Eigen::Isometry3d matrix;
    matrix.setIdentity();
    matrix.translation() = position;
    matrix.linear() = orientation.normalized().toRotationMatrix();
    return matrix;
}

//! Convert a homogeneous transformation matrix to position and orientation.
std::pair<Eigen::Vector3d, Eigen::Quaterniond> Robothon::Planner::mat2posRot(const Eigen::Isometry3d& matrix)
{
    Eigen::Vector3d position = matrix.translation();
    Eigen::Quaterniond rotation = Eigen::Quaterniond(matrix.linear());
    return std::make_pair(position, rotation);
}

//! Transforms poses defined for the TCP w.r.t. the base into poses for panda_link8, i.e. the flange,  w.r.t. the
//! base.
Eigen::Isometry3d Robothon::Planner::tcpToFlangeTransform(const Eigen::Isometry3d& tcpPoseInBase)
{
    // Constant transformation between TCP and flange.
    // W.r.t. flange, the TCP is shifted to positive z-direction.
    // The TCP frame (which is defined by us) is rotated by -45 degrees around z-axis w.r.t. the flange.
    Eigen::Isometry3d tcpInFlange = posRot2mat((Eigen::Matrix<double, 3, 1>() << 0.0, 0.0, 0.1034).finished(),
        Eigen::Quaterniond(0.924, 0.0, 0.0, -0.383).normalized());

    // Calculate the transformation for the flange.
    return tcpPoseInBase * tcpInFlange.inverse();
}

//! Transforms the stiffness vector w.r.t. taskboard frame to base frame and returns the reoriented stiffness as
//! std::vector.
std::vector<double> Robothon::Planner::transformKpImp(
    std::vector<double>& kpImpInBoardRef, const Eigen::Isometry3d& boardRefInBase)
{
    Eigen::Matrix<double, 6, 1> stiffnessInBoardRefVec(kpImpInBoardRef.data());
    Eigen::Matrix<double, 6, 6> stiffnessInBoardRef = stiffnessInBoardRefVec.asDiagonal();
    const Eigen::Matrix<double, 3, 3> stiffnessTransInBaseFrameMat
        = boardRefInBase.linear().transpose() * stiffnessInBoardRef.block<3, 3>(0, 0) * boardRefInBase.linear();
    const Eigen::Matrix<double, 3, 1> stiffnessTransInBaseFrame = stiffnessTransInBaseFrameMat.diagonal().array().abs();
    const Eigen::Matrix<double, 3, 3> stiffnessRotInBaseFrameMat
        = boardRefInBase.linear().transpose() * stiffnessInBoardRef.block<3, 3>(3, 3) * boardRefInBase.linear();
    const Eigen::Matrix<double, 3, 1> stiffnessRotInBaseFrame = stiffnessRotInBaseFrameMat.diagonal().array().abs();
    std::vector<double> kpImp;
    kpImp.push_back(stiffnessTransInBaseFrame(0));
    kpImp.push_back(stiffnessTransInBaseFrame(1));
    kpImp.push_back(stiffnessTransInBaseFrame(2));
    kpImp.push_back(stiffnessRotInBaseFrame(0));
    kpImp.push_back(stiffnessRotInBaseFrame(1));
    kpImp.push_back(stiffnessRotInBaseFrame(2));
    return kpImp;
}

//! Transforms the desired force vector w.r.t. taskboard frame to base frame and returns the reoriented desired force as
//! std::vector.
std::vector<double> Robothon::Planner::transformForce(
    std::vector<double>& forceInBoardRef, const Eigen::Isometry3d& boardRefInBase)
{
    Eigen::Matrix<double, 6, 1> forceInBoardRefEig;
    forceInBoardRefEig << forceInBoardRef[0], forceInBoardRef[1], forceInBoardRef[2], forceInBoardRef[3],
        forceInBoardRef[4], forceInBoardRef[5];
    Eigen::Matrix<double, 3, 1> forceTransInBaseMat = boardRefInBase.linear() * forceInBoardRefEig.block(0, 0, 3, 1);
    Eigen::Matrix<double, 3, 1> forceRotInBaseMat = boardRefInBase.linear() * forceInBoardRefEig.block(3, 0, 3, 1);
    std::vector<double> forceInBase;
    forceInBase.push_back(forceTransInBaseMat(0));
    forceInBase.push_back(forceTransInBaseMat(1));
    forceInBase.push_back(forceTransInBaseMat(2));
    forceInBase.push_back(forceRotInBaseMat(0));
    forceInBase.push_back(forceRotInBaseMat(1));
    forceInBase.push_back(forceRotInBaseMat(2));
    return forceInBase;
}
//! Rotates params around a certain angle and returns the absolute values.
std::tuple<double, double> Robothon::Planner::transformWiggle(double wiggleX, double wiggleY, double zRotationAngle)
{
    return { std::abs(wiggleX * std::cos(zRotationAngle) + wiggleY * std::sin(zRotationAngle)),
        std::abs(wiggleX * std::sin(zRotationAngle) + wiggleY * std::cos(zRotationAngle)) };
}
