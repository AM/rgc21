/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/MotionPlanner.hpp"
#include "motion_planner/environment/PrimitiveEnvironmentObject.hpp"
#include "motion_planner/environment/StaticObjectType.hpp"
#include "motion_planner/utils/MotionPlannerConfig.hpp"
#include "motion_planner/utils/utility_functions.hpp"
#include <Eigen/Geometry>
#include <chrono>
#include <cmath>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>

//! Special Constructor.
Robothon::Planner::MotionPlanner::MotionPlanner(ros::NodeHandle& nh)
    : m_moveGroupInterface{ "panda_arm" }
    , m_robotModelLoader{ "robot_description" }
    , m_kinematicModel{ new moveit::core::RobotState(m_robotModelLoader.getModel()) }
    , m_jointModelGroup{ std::make_unique<const moveit::core::JointModelGroup>(
          *(m_moveGroupInterface.getCurrentState()->getJointModelGroup("panda_arm"))) }
    , m_frankaStateSubscriber{ nh.subscribe(
          "/franka_state_controller/franka_states", 1000, &MotionPlanner::frankaStateSubscriberCallback, this) }
    , m_graspClient{ "franka_gripper/grasp", true }
    , m_addObjectServer{ nh.advertiseService("addObject", &MotionPlanner::addObjectServiceCallback, this) }
    , m_getLinkEightFrameInBaseServer{ nh.advertiseService(
          "getLinkEightFrameInBase", &MotionPlanner::getLinkEightFrameInBaseServiceCallback, this) }
    , m_moveJointSpaceServer{ nh.advertiseService(
          "moveJointSpace", &MotionPlanner::moveJointSpaceServiceCallback, this) }
    , m_moveTaskSpaceServer{ nh.advertiseService("moveTaskSpace", &MotionPlanner::moveTaskSpaceServiceCallback, this) }
    , m_PCACServer{ nh.advertiseService(
          "parameterizableCompliantAssemblyControl", &MotionPlanner::PCACServiceCallback, this) }
    , m_controllerManager{ nh }
    , m_logger{ "robothon_log/wrench_time.txt" }
    , m_taskSupervisor{ m_endEffectorState, m_endEffectorStateMutex }
{
    m_graspClient.waitForServer();
    m_logger.openLogfile(
        "Time;EEWrenchBFrame0;EEWrenchBFrame1;EEWrenchBFrame2;EEWrenchKFrame3;EEWrenchKFrame4;EEWrenchKFrame5;"
        "PosX;PosY;PosZ;dq0;dq1;dq2;dq3;dq4;dq5;dq6;EEWrenchBFrame0;EEWrenchBFrame1;EEWrenchBFrame2;EEWrenchBFrame3;"
        "EEWrenchBFrame4;EEWrenchBFrame5");
}

//! Callback function of the \ref m_frankaStateSubscriber.
void Robothon::Planner::MotionPlanner::frankaStateSubscriberCallback(const franka_msgs::FrankaState::ConstPtr& msg)
{
    const std::lock_guard<std::mutex> lock(m_endEffectorStateMutex);
    m_endEffectorState.m_externalWrenchBaseFrame << msg->O_F_ext_hat_K[0], msg->O_F_ext_hat_K[1], msg->O_F_ext_hat_K[2],
        msg->O_F_ext_hat_K[3], msg->O_F_ext_hat_K[4], msg->O_F_ext_hat_K[5];
    m_endEffectorState.m_externalWrenchKFrame << msg->K_F_ext_hat_K[0], msg->K_F_ext_hat_K[1], msg->K_F_ext_hat_K[2],
        msg->K_F_ext_hat_K[3], msg->K_F_ext_hat_K[4], msg->K_F_ext_hat_K[5];
    m_endEffectorState.m_eePosition << msg->O_T_EE[12], msg->O_T_EE[13], msg->O_T_EE[14];
    m_endEffectorState.m_eeRotation << msg->O_T_EE[0], msg->O_T_EE[4], msg->O_T_EE[8], msg->O_T_EE[1], msg->O_T_EE[5],
        msg->O_T_EE[9], msg->O_T_EE[2], msg->O_T_EE[6], msg->O_T_EE[10];
    // Write some values to the log file.
    m_logger.writeToLogfile(std::array<double, 22>(
        { m_endEffectorState.m_externalWrenchBaseFrame[0], m_endEffectorState.m_externalWrenchBaseFrame[1],
            m_endEffectorState.m_externalWrenchBaseFrame[2], m_endEffectorState.m_externalWrenchKFrame[3],
            m_endEffectorState.m_externalWrenchKFrame[4], m_endEffectorState.m_externalWrenchKFrame[5], msg->O_T_EE[12],
            msg->O_T_EE[13], msg->O_T_EE[14], msg->dq[0], msg->dq[1], msg->dq[2], msg->dq[3], msg->dq[4], msg->dq[5],
            msg->dq[6], msg->O_F_ext_hat_K[0], msg->O_F_ext_hat_K[1], msg->O_F_ext_hat_K[2], msg->O_F_ext_hat_K[3],
            msg->O_F_ext_hat_K[4], msg->O_F_ext_hat_K[5] }));
}

//! Callback function of the \ref m_addObjectServer.
bool Robothon::Planner::MotionPlanner::addObjectServiceCallback(
    motion_planner::AddObject::Request& req, motion_planner::AddObject::Response& /*res*/)
{
    // Generate the environment object based on primitive geometries.
    PrimitiveEnvironmentObject primitiveEnvironmentObject(static_cast<StaticObjectType>(req.id));
    // Move the environment object's shapes to the desired location and add the shapes to the planning scene.
    moveit_msgs::CollisionObject planningSceneObject;
    auto [position, orientation] = getEigenPose(req.pose);
    std::tie(planningSceneObject.primitives, planningSceneObject.primitive_poses)
        = primitiveEnvironmentObject.generateRosMessage(position, orientation);
    planningSceneObject.id = primitiveEnvironmentObject.getName();
    planningSceneObject.header.frame_id = m_moveGroupInterface.getPlanningFrame();
    planningSceneObject.operation = planningSceneObject.ADD;
    m_planningSceneInterface.applyCollisionObject(planningSceneObject);
    // Modify the AllowedCollisionMatrix.
    // Get a locked planning scene to update the allowed collisions.
    auto planningSceneMonitor = std::make_shared<planning_scene_monitor::PlanningSceneMonitor>("robot_description");
    planningSceneMonitor->requestPlanningSceneState("/get_planning_scene");
    planning_scene_monitor::LockedPlanningSceneRW planningScene(planningSceneMonitor);
    // Update the AllowedCollisionMatrix.
    for (const auto& pair : MotionPlannerConfig::get().m_allowedCollisionPairs) {
        planningScene->getAllowedCollisionMatrixNonConst().setEntry(pair.first, pair.second, true);
    }
    for (const auto& element : MotionPlannerConfig::get().m_allowedCollisionElement) {
        planningScene->getAllowedCollisionMatrixNonConst().setEntry(element, true);
    }
    // Format the updated planning scene to a ROS message that is used to update the scene via the
    // PlanningSceneInterface.
    moveit_msgs::PlanningScene updatedPlaningScene;
    planningScene->getPlanningSceneMsg(updatedPlaningScene);
    m_planningSceneInterface.applyPlanningScene(updatedPlaningScene);
    return true;
}

//! ROS service callback to get the link 8 transform.
bool Robothon::Planner::MotionPlanner::getLinkEightFrameInBaseServiceCallback(
    motion_planner::GetLinkEightFrameInBase::Request& /*req*/, motion_planner::GetLinkEightFrameInBase::Response& res)
{
    const std::lock_guard<std::mutex> lock(m_endEffectorStateMutex);
    Eigen::Isometry3d eeFrameInBase = Robothon::Planner::posRot2mat(
        m_endEffectorState.m_eePosition, Eigen::Quaterniond(m_endEffectorState.m_eeRotation));
    Eigen::Vector3d eeFrameInLink8Trans;
    eeFrameInLink8Trans << 0.0, 0.0, 0.1034;
    Eigen::Isometry3d eeFrameInLinkEight
        = Robothon::Planner::posRot2mat(eeFrameInLink8Trans, Eigen::Quaterniond(0.924, 0.0, 0.0, -0.383));
    Eigen::Isometry3d link8InBase = eeFrameInBase * eeFrameInLinkEight.inverse();
    res.linkEightInBase = Robothon::Planner::getRosPose(link8InBase);
    return true;
}

//! Callback function of the \ref m_moveJointSpaceServer.
bool Robothon::Planner::MotionPlanner::moveJointSpaceServiceCallback(
    motion_planner::MoveJointSpace::Request& req, motion_planner::MoveJointSpace::Response& res)
{
    performJointSpaceMotion(req.goal, res);
    return true;
}

//! Callback function of the \ref m_moveRobotServer.
bool Robothon::Planner::MotionPlanner::moveTaskSpaceServiceCallback(
    motion_planner::MoveTaskSpace::Request& req, motion_planner::MoveTaskSpace::Response& res)
{
    auto [positionTcp, orientationTcp] = getEigenPose(req.goal);
    // The provided goal is formulated as a desired position and orientation of the end effector. Planning, however, is
    // done for the panda_link8 frame. Therefore, the provided position and orientation are transformed such that the
    // returned service includes the desired pose for the flange frame (panda_link8), not the end effector.
    Eigen::Isometry3d desiredTcpPose = posRot2mat(positionTcp, orientationTcp.normalized());
    Eigen::Isometry3d goalPose = tcpToFlangeTransform(desiredTcpPose);
    // MoveIt!'s  internal planner behave bad in task space. The sampled IK solution for the goal is often not feasible.
    // The approach is thus, to first compute a proper IK solution for the provided task space goal. Afterwards,
    // planning is done in joint space. The computation of the IK solution for the goal pose uses and initial guess.
    // Thereby, the robot's first joint is oriented towards the goal within the x-y-plane. The other joints have their
    // home configuration.
    Eigen::Matrix<double, 7, 1> initialIkSeed = MotionPlannerConfig::get().m_homeConfiguration;
    initialIkSeed[0] = std::atan2(goalPose.translation()[1], goalPose.translation()[0]);
    // Compute IK.
    m_kinematicModel->setJointGroupPositions(m_jointModelGroup.get(), initialIkSeed);
    m_kinematicModel->update();

    int ikCounter = 0;
    std::vector<double> configuration;
    if (!m_kinematicModel->setFromIK(m_jointModelGroup.get(), goalPose)) {
        // Compute other initial seed for the IK computation.
        while (!m_kinematicModel->setFromIK(m_jointModelGroup.get(), goalPose)) {
            m_kinematicModel->setToRandomPositions(m_jointModelGroup.get());
            m_kinematicModel->update();
            m_kinematicModel->copyJointGroupPositions(m_jointModelGroup.get(), configuration);
            ROS_WARN_STREAM(++ikCounter << " IK trial with configuration: ");
            for (auto& el : configuration) {
                ROS_WARN_STREAM(el);
            }
        }
    }
    std::vector<double> ikSolution;
    m_kinematicModel->copyJointGroupPositions(m_jointModelGroup.get(), ikSolution);
    // Perform a joint space motion to the goal.
    performJointSpaceMotion(ikSolution, res);
    return true;
}

//! Callback function of the \ref m_PCACServer.
bool Robothon::Planner::MotionPlanner::PCACServiceCallback(
    motion_planner::ParameterizableCompliantAssemblyControl::Request& req,
    motion_planner::ParameterizableCompliantAssemblyControl::Response& res)
{
    // Activate Cartesian impedance controller.
    if (!activatePCAC(req)) {
        res.success = false;
        return true;
    }
    // Terminate PCAC when task is completed.
    res.success = m_taskSupervisor.superviseTask(req);
    // Stop the controller.
    if (!m_controllerManager.switchController("")) {
        ROS_ERROR("MotionPlanner.cpp: Unable to stop the PCAC!");
        res.success = false;
    }
    return true;
}
