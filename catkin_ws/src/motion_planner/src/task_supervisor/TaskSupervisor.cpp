/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/task_supervisor/TaskSupervisor.hpp"
#include <Eigen/Geometry>
#include <ros/console.h>
#include <thread>

//! Specialized Constructor.
Robothon::Planner::TaskSupervisor::TaskSupervisor(EndEffectorState& endEffectorState, std::mutex& endEffectorStateMutex)
    : m_refEndEffectorState{ endEffectorState }
    , m_refEndEffectorStateMutex{ endEffectorStateMutex }
{
}

//! Checks whether a task is successfully finished.
bool Robothon::Planner::TaskSupervisor::superviseTask(
    const motion_planner::ParameterizableCompliantAssemblyControl::Request& request) const
{
    // Wait until the user stops the controller.
    switch (request.taskID) {
    case 0:
        return checkButtonTask(request.desiredForce.at(2)); // Push force in z-direction.
    case 1:
        return checkInsertKeyTask();
    case 2:
        return checkTurnKeyTask();
    case 3:
        return checkPickLanTask();
    case 4:
        return checkInsertLanTask();
    case 5:
        return checkOpenLidTask();
    case 6:
        return checkRemoveLidTask();
    case 7:
    case 9:
        return checkRemoveBatteryTask();
    case 8:
        return checkInsertBatteryTask();
    default:
        ROS_ERROR("TaskSupervisor.cpp: Termination Criteria not implemented yet!");
    }
    return false;
}

//! Check for the button task.
bool Robothon::Planner::TaskSupervisor::checkButtonTask(double desiredForce) const
{
    // Wait until the push force is achieved in z-direction.
    bool stopController = false;
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        stopController
            = std::abs(m_refEndEffectorState.m_externalWrenchBaseFrame[2]) > std::abs(desiredForce); // NoTransformation
        m_refEndEffectorStateMutex.unlock();
        // Waiting to allow updates via the m_refEndEffectorStateSubscriber's callback.
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    return true;
}

//! Check for the key insertion.
bool Robothon::Planner::TaskSupervisor::checkInsertKeyTask() const
{
    // Parameters to count the time for each insert try.
    double maxTime = 6.0;
    auto startTime = std::chrono::high_resolution_clock::now();
    bool timeFailure = false;
    // Wait until the push force is achieved and the gripper is approximately at the turn position.
    bool taskSuccess = false;
    while (!taskSuccess) {
        m_refEndEffectorStateMutex.lock();
        taskSuccess = (m_refEndEffectorState.m_eePosition[2] < 0.012)
            && (std::abs(m_refEndEffectorState.m_externalWrenchBaseFrame[2]) > 3.5); // NoTransformation
        m_refEndEffectorStateMutex.unlock();
        // Waiting to allow updates via the m_refEndEffectorStateSubscriber's callback.
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        // Count the time
        auto endTime = std::chrono::high_resolution_clock::now();
        if (std::chrono::duration<double, std::milli>(endTime - startTime).count() / 1000 > maxTime) {
            return false; // Time Failure.
        }
    }
    return true;
}

//! Check for turning the key.
bool Robothon::Planner::TaskSupervisor::checkTurnKeyTask() const
{
    bool stopController = false;
    m_refEndEffectorStateMutex.lock();
    Eigen::Vector3d initialRotation = m_refEndEffectorState.m_eeRotation.eulerAngles(0, 1, 2);
    m_refEndEffectorStateMutex.unlock();
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        Eigen::Vector3d currentRotation = m_refEndEffectorState.m_eeRotation.eulerAngles(0, 1, 2);
        m_refEndEffectorStateMutex.unlock();
        stopController = std::abs(initialRotation[2] - currentRotation[2]) > M_PI / 4;
    }
    return true;
}

//! Check for picking up the LAN cable.
bool Robothon::Planner::TaskSupervisor::checkPickLanTask() const
{
    // Wait until the push force is achieved.
    m_refEndEffectorStateMutex.lock();
    Eigen::Vector3d initialPosition = m_refEndEffectorState.m_eePosition;
    m_refEndEffectorStateMutex.unlock();
    Eigen::Vector3d currentPosition;
    bool stopController = false;
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        currentPosition = m_refEndEffectorState.m_eePosition;
        m_refEndEffectorStateMutex.unlock();
        stopController = (currentPosition - initialPosition).norm() > 0.02;
        // Waiting to allow updates via the m_refEndEffectorStateSubscriber's callback.
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    return true;
}

//! Check for inserting the LAN cable.
bool Robothon::Planner::TaskSupervisor::checkInsertLanTask() const
{
    // Parameters to count the time for each insertion trial.
    double maxTime = 6.0;
    auto startTime = std::chrono::high_resolution_clock::now();
    bool timeFailure = false;
    // Wait until the LAN reaches a defined z-coordinate.
    bool taskSuccess = false;
    while (!taskSuccess) {
        m_refEndEffectorStateMutex.lock();
        taskSuccess = m_refEndEffectorState.m_eePosition[2] < 0.0045;
        m_refEndEffectorStateMutex.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        auto endTime = std::chrono::high_resolution_clock::now();
        if (std::chrono::duration<double, std::milli>(endTime - startTime).count() / 1000 > maxTime) {
            return false; // Time Failure.
        }
    }
    return true;
}

//! Check the opening of the lid.
bool Robothon::Planner::TaskSupervisor::checkOpenLidTask() const
{
    m_refEndEffectorStateMutex.lock();
    Eigen::Vector3d initialPosition = m_refEndEffectorState.m_eePosition;
    m_refEndEffectorStateMutex.unlock();
    Eigen::Vector3d currentPosition;
    bool stopController = false;
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        currentPosition = m_refEndEffectorState.m_eePosition;
        m_refEndEffectorStateMutex.unlock();
        stopController = (currentPosition - initialPosition).norm() > 0.02;
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    return true;
}

//! Check the removal of the lid.
bool Robothon::Planner::TaskSupervisor::checkRemoveLidTask() const
{
    // Stop the controller when a certain y-position difference w.r.t. taskboard frame is reached.
    m_refEndEffectorStateMutex.lock();
    Eigen::Vector3d initialPosition = m_refEndEffectorState.m_eePosition;
    m_refEndEffectorStateMutex.unlock();
    Eigen::Vector3d currentPosition;
    bool stopController = false;
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        currentPosition = m_refEndEffectorState.m_eePosition;
        m_refEndEffectorStateMutex.unlock();
        stopController = (currentPosition - initialPosition).norm() > 0.08;
        // Waiting to allow updates via the m_refEndEffectorStateSubscriber's callback.
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    return true;
}

//! Check the removal of the battery.
bool Robothon::Planner::TaskSupervisor::checkRemoveBatteryTask() const
{
    // Wait until the push force is achieved.
    m_refEndEffectorStateMutex.lock();
    Eigen::Vector3d initialPosition = m_refEndEffectorState.m_eePosition;
    m_refEndEffectorStateMutex.unlock();
    Eigen::Vector3d currentPosition;
    bool stopController = false;
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        currentPosition = m_refEndEffectorState.m_eePosition;
        m_refEndEffectorStateMutex.unlock();
        // Observe if the end-effector is above a certain z-value.
        if (std::abs(currentPosition[2] - initialPosition[2]) > 0.01) {
            stopController = true;
        }
        // Waiting to allow updates via the m_refEndEffectorStateSubscriber's callback.
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    return true;
}

//! Check the insertion of the battery.
bool Robothon::Planner::TaskSupervisor::checkInsertBatteryTask() const
{
    // Wait until the push force is achieved.
    bool stopController = false;
    while (!stopController) {
        m_refEndEffectorStateMutex.lock();
        if (m_refEndEffectorState.m_eePosition[2] < 0.006) {
            stopController = true;
        }
        m_refEndEffectorStateMutex.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
    return true;
}
