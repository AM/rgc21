/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include "motion_planner/ParameterizableCompliantAssemblyControl.h"
#include "motion_planner/utils/EndEffectorState.hpp"
#include <mutex>

namespace Robothon::Planner {

//! Checks whether a task is successfully finished by the PCAC.
/*!
 * Currently a lot of hard coded. ToDo.
 */
class TaskSupervisor {
public:
    //! Specialized Constructor.
    /*!
     * During the check for task success, the TaskSupervisor access the end effector state in the MotionPlanner.
     *
     * @param endEffectorState End effector state.
     * @param endEffectorStateMutex Mutex, that protects the end effector state.
     */
    explicit TaskSupervisor(EndEffectorState& endEffectorState, std::mutex& endEffectorStateMutex);

    //! Checks whether a task is successfully finished.
    /*!
     * According to the taskID the correct task check is called.
     *
     * @param request Contains information about the desired PCAC parameters and the taskID.
     * @return Success flag.
     */
    bool superviseTask(const motion_planner::ParameterizableCompliantAssemblyControl::Request& request) const;

private:
    //! Reference to the end effector state in the MotionPlanner.
    EndEffectorState& m_refEndEffectorState;

    //! Reference to the mutex that protects the end effector state in the MotionPlanner.
    std::mutex& m_refEndEffectorStateMutex;

    //! Check for the button task.
    bool checkButtonTask(double desiredForce) const;

    //! Check for the key insertion.
    bool checkInsertKeyTask() const;

    //! Check for turning the key.
    bool checkTurnKeyTask() const;

    //! Check for picking up the LAN cable.
    bool checkPickLanTask() const;

    //! Check for inserting the LAN cable.
    bool checkInsertLanTask() const;

    //! Check the opening of the lid.
    bool checkOpenLidTask() const;

    //! Check the removal of the lid.
    bool checkRemoveLidTask() const;

    //! Check the removal of the battery.
    bool checkRemoveBatteryTask() const;

    //! Check the insertion of the battery.
    bool checkInsertBatteryTask() const;
};
} // namespace Robothon::Planner