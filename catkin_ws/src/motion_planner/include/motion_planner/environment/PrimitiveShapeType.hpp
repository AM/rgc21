/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

namespace Robothon::Planner {
//! Primitive shape type available for environment modeling in MoveIt!.
/*!
 * See <a href="http://docs.ros.org/en/melodic/api/shape_msgs/html/msg/SolidPrimitive.html">here</a>.
 */
enum class PrimitiveShapeType { BOX = 1, SPHERE = 2, CYLINDER = 3, CONE = 4 };
} // namespace Robothon::Planner