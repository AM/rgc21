/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include "motion_planner/environment/PrimitiveShapeType.hpp"
#include "motion_planner/environment/StaticObjectType.hpp"
#include "motion_planner/utils/utility_functions.hpp"
#include <Eigen/Geometry>
#include <XmlRpcValue.h>
#include <geometry_msgs/Pose.h>
#include <moveit_msgs/CollisionObject.h>
#include <shape_msgs/SolidPrimitive.h>
#include <string>
#include <tuple>
#include <vector>

namespace Robothon::Planner {
//! Generates environment objects based on geometric primitives.
/*!
 * Reads the geometric properties of static environment objects from the ROS Parameter Server, generate the
 * corresponding representations via geometric primitives and provides an interface for generating ROS messages to
 * add the objects to the PlanningScene.
 */
class PrimitiveEnvironmentObject {
public:
    //! Specialized constructor.
    /*!
     * Reads out the geometric information of the environment object of the corresponding yaml file in the
     * motion_planner/config/ folder.
     *
     * @param object Object type to be added to the environment (e.g. table etc.).
     */
    explicit PrimitiveEnvironmentObject(StaticObjectType object);

    //! Generates corresponding ROS message that defines the shapes forming the object.
    /*!
     * See <a href="http://docs.ros.org/en/melodic/api/moveit_msgs/html/msg/CollisionObject.html">here</a>. Generates
     * the shapes_msgs/SolidPrimitive[] and geometry_msgs/Pose[] part of the ROS message. Performs the transformations
     * on the shapes that form the environment object.
     *
     * @param position Position of the object w.r.t. to the robot's base frame.
     * @param orientation Orientation of the object w.r.t. to the robot's base frame.
     * @return ROS message types.
     */
    std::tuple<std::vector<shape_msgs::SolidPrimitive>, std::vector<geometry_msgs::Pose>> generateRosMessage(
        const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation);

    //! Getter for \ref m_name.
    std::string getName() { return m_name; }

private:
    //! Primitive shape with associated geometric information.
    /*!
     * For further details on the geometries see
     * <a href="http://docs.ros.org/en/melodic/api/shape_msgs/html/msg/SolidPrimitive.html">here</a>. Nested class
     * to hide the implementation details.
     */
    struct PrimitiveShape {
        //! Generate primitive shapes based on ROS Parameter Server information.
        /*!
         * @param shapeInfo Geometric properties of the primitive shape as stored on the ROS Parameter Server.
         * @param safetyDistance Safety distance that is added to the primitive shape in the xy-plane in [m].
         */
        PrimitiveShape(const XmlRpc::XmlRpcValue& shapeInfo, double safetyDistance)
        {
            // Get the shape type.
            m_primitiveShapeType = static_cast<PrimitiveShapeType>(static_cast<int>(shapeInfo["type"]));
            // Get the dimension.
            auto dim = shapeInfo["dimension"];
            for (int kk = 0; kk < dim.size(); ++kk) {
                m_dimensions.emplace_back(static_cast<double>(dim[kk]));
            }
            // Apply safety distance in xy-plane.
            switch (m_primitiveShapeType) {
            case PrimitiveShapeType::BOX:
                m_dimensions.at(0) += safetyDistance;
                m_dimensions.at(1) += safetyDistance;
                break;
            case PrimitiveShapeType::CYLINDER:
                m_dimensions.at(1) += safetyDistance;
                break;
            }
            // Get the shape's position w.r.t. the object frame.
            m_positionInObjectFrame = (Eigen::Vector3d() << static_cast<double>(shapeInfo["position"][0]),
                static_cast<double>(shapeInfo["position"][1]), static_cast<double>(shapeInfo["position"][2]))
                                          .finished();
            // Get the shape's orientation w.r.t. the object frame.
            m_orientationInObjectFrame = Eigen::Quaterniond(static_cast<double>(shapeInfo["orientation"][0]),
                static_cast<double>(shapeInfo["orientation"][1]), static_cast<double>(shapeInfo["orientation"][2]),
                static_cast<double>(shapeInfo["orientation"][3]));
        }
        //! Position of the shape in w.r.t. the object frame.
        Eigen::Vector3d m_positionInObjectFrame;
        //! Orientation of the shape w.r.t. the object frame.
        Eigen::Quaterniond m_orientationInObjectFrame;
        //! Type of the primitive (box, sphere etc.).
        PrimitiveShapeType m_primitiveShapeType;
        //! Dimensions depending on the type, i.e. \ref on m_primitiveShapeType.
        std::vector<double> m_dimensions;
    };

    //! Primitive shapes forming the environment object.
    /*!
     * See PrimitiveShape.
     */
    std::vector<PrimitiveShape> m_primitiveShapes;

    //! Name of the current environment object.
    std::string m_name;
};
} // namespace Robothon::Planner