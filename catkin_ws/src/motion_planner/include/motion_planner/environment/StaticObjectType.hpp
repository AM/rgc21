/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

namespace Robothon::Planner {
//! Static objects in the environment during the Robothon 2021.
enum class StaticObjectType {
    TABLE, // Table on which the task board is mounted.
    TASK_BOARD, // Task board that has to be solved
};
} // namespace Robothon::Planner
