/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include "motion_planner/AddObject.h"
#include "motion_planner/GetLinkEightFrameInBase.h"
#include "motion_planner/MoveJointSpace.h"
#include "motion_planner/MoveTaskSpace.h"
#include "motion_planner/ParameterizableCompliantAssemblyControl.h"
#include "motion_planner/control/ControllerManager.hpp"
#include "motion_planner/log/Logger.h"
#include "motion_planner/task_supervisor/TaskSupervisor.hpp"
#include "motion_planner/utils/EndEffectorState.hpp"
#include <franka_gripper/GraspAction.h>
#include <franka_msgs/FrankaState.h>
#include <memory>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <mutex>
#include <ros/node_handle.h>

namespace Robothon::Planner {
//! Motion planner class.
/*!
 * The MotionPlanner is responsible for controlling the robot. It provides ROS servers for both moving the robot and for
 * calling the PCAC. Planning motions within this class is based on
 * <a href="http://docs.ros.org/en/melodic/api/moveit_tutorials/html/index.html">MoveIt!</a>.
 */
class MotionPlanner {
public:
    //! Special Constructor.
    /*!
     * The constructor instantiates the provided ROS servers and loads the MoveIt! interface components for path
     * planning, kinematics etc.
     *
     * @param nh Handle to the process' node in which the servers are instantiated.
     */
    explicit MotionPlanner(ros::NodeHandle& nh);

private:
    //! Current robot state updated by the \ref m_frankaStateSubscriber's callback.
    EndEffectorState m_endEffectorState;

    //! Mutex to protect the RobotState data.
    /*!
     * The RobotState data is is written by the \ref m_frankaStateSubscriber's callback and accessed by the callbacks of
     * the controllers to decide whether the controller might be stopped.
     */
    std::mutex m_endEffectorStateMutex;

    //! Controller manager to switch between controllers for different tasks (plug-in, motion etc.).
    ControllerManager m_controllerManager;

    //! Task supervisor that determines when to stop the PCAC, i.e. when a task is completed.
    TaskSupervisor m_taskSupervisor;

    //! Logger object for the MotionPlanner.
    Logger m_logger;

    //! Client class to conveniently use the ROS interfaces for motion planning.
    moveit::planning_interface::MoveGroupInterface m_moveGroupInterface;

    //! Looks up the robot model on the parameter server and instantiates a robot model that is used by the
    //! \ref m_kinematicModel.
    /*!
     * Throughout the lifetime of the \ref m_kinematicModel there must exist a corresponding RobotModel.
     */
    robot_model_loader::RobotModelLoader m_robotModelLoader;

    //! Kinematic model of the robot.
    moveit::core::RobotStatePtr m_kinematicModel;

    //! Raw pointers are frequently used to refer to the planning group for improved performance.
    std::unique_ptr<const moveit::core::JointModelGroup> m_jointModelGroup;

    //! Environment model interface provided by MoveIt!.
    /*!
     * Alternative options to interface with the environment model, i.e. the PlanningScene, are the
     * PlanningSceneMonitor and instantiating the PlanningScene by yourself (not recommended).
     */
    moveit::planning_interface::PlanningSceneInterface m_planningSceneInterface;

    //! ROS subscriber to read the robot states during running a controller.
    ros::Subscriber m_frankaStateSubscriber;

    //! ROS service client to grasped.
    actionlib::SimpleActionClient<franka_gripper::GraspAction> m_graspClient;

    //! ROS server to add an object to the environment model.
    ros::ServiceServer m_addObjectServer;

    //! ROS server to get the link 8 transform.
    ros::ServiceServer m_getLinkEightFrameInBaseServer;

    //! ROS server to move the robot to a joint space goal.
    ros::ServiceServer m_moveJointSpaceServer;

    //! ROS server to move the robot's end effector to a task space pose.
    ros::ServiceServer m_moveTaskSpaceServer;

    //! ROS server to test the PCAC.
    ros::ServiceServer m_PCACServer;

    //! Callback function of the \ref m_frankaStateSubscriber.
    /*!
     * @param msg Ros message published by a realtime_tools publisher within the the franka_state_controller that is
     * launched by franka_control.
     */
    void frankaStateSubscriberCallback(const franka_msgs::FrankaState::ConstPtr& msg);

    //! Callback function of the \ref m_addObjectServer.
    /*!
     * @param req Service request information as defined in the *.srv file.
     * @param res Service response information as defined in the *.srv file.
     * @return Returns always true to indicate a successful service call (though the service itself might have
     * failed).
     */
    bool addObjectServiceCallback(motion_planner::AddObject::Request& req, motion_planner::AddObject::Response& res);

    //! Callback function of the \ref m_getLinkEightFrameInBase.
    /*!
     * See \ref addObjectServiceCallback.
     */
    bool getLinkEightFrameInBaseServiceCallback(
        motion_planner::GetLinkEightFrameInBase::Request& req, motion_planner::GetLinkEightFrameInBase::Response& res);

    //! Callback function of the \ref m_moveJointSpaceServer.
    /*!
     * See \ref addObjectServiceCallback.
     */
    bool moveJointSpaceServiceCallback(
        motion_planner::MoveJointSpace::Request& req, motion_planner::MoveJointSpace::Response& res);

    //! Callback function of the \ref m_moveTaskSpaceServer.
    /*!
     * See \ref addObjectServiceCallback.
     */
    bool moveTaskSpaceServiceCallback(
        motion_planner::MoveTaskSpace::Request& req, motion_planner::MoveTaskSpace::Response& res);

    //! Callback function of the \ref m_PCACServer.
    /*!
     * See \ref addObjectServiceCallback.
     */
    bool PCACServiceCallback(motion_planner::ParameterizableCompliantAssemblyControl::Request& req,
        motion_planner::ParameterizableCompliantAssemblyControl::Response& res);

    //! Performs a joint space motion to the provided goal.
    /*!
     * Both the \ref m_moveJointSpaceServer and the \ref m_moveTaskSpaceServer plan in joint space an perform the motion
     * in joint space (see comments in \ref moveTaskSpaceServiceCallback implementation). As they have different service
     * response types, the function is templated.
     *
     * @tparam T Service response type.
     * @param goal Desired joint angles of the goal configuration in [rad].
     * @param serviceResponse Service response that stores the success flag.
     */
    template <typename T> void performJointSpaceMotion(const std::vector<double>& goal, T& serviceResponse);

    //! Activates the PCAC with the provided parameters.
    /*!
     * The service callbacks are often based on the PCAC. This function first sets the desired
     * controller parameters on the ROS parameter server and activates the controller afterwards.
     *
     * @tparam T Service request type that has to provide the desired parameters.
     * @param parameters The request must contain Kp_imp, Kp_force, Ki_force and desiredForce.
     * @return Success flag.
     */
    template <typename T> bool activatePCAC(const T& parameters);
};

//! Performs a joint space motion to the provided goal.
template <typename T> void MotionPlanner::performJointSpaceMotion(const std::vector<double>& goal, T& serviceResponse)
{
    // Switch the controller accordingly.
    if (!m_controllerManager.switchController("position_joint_trajectory_controller")) {
        ROS_ERROR("MotionPlanner.cpp: Unable to switch to position_joint_trajectory_controller!");
        serviceResponse.success = false;
        return;
    }
    // Perform the motion.
    m_moveGroupInterface.setJointValueTarget(goal);
    moveit::planning_interface::MoveGroupInterface::Plan plan;
    if (m_moveGroupInterface.plan(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS) {
        moveit_visual_tools::MoveItVisualTools visualTools("panda_link0");
        visualTools.loadRemoteControl();
        visualTools.publishTrajectoryLine(plan.trajectory_, m_jointModelGroup.get());
        visualTools.trigger();
        serviceResponse.success
            = (m_moveGroupInterface.execute(plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    } else {
        ROS_ERROR("MotionPlanner.cpp: Could not plan to the provided joint space goal!");
        serviceResponse.success = false;
    }
}

//! Activates the PCAC with the provided parameters.
template <typename T> bool MotionPlanner::activatePCAC(const T& parameters)
{
    ros::param::set("/robothon/online/Kp_imp", parameters.Kp_imp);
    ros::param::set("/robothon/online/Kp_force", parameters.Kp_force);
    ros::param::set("/robothon/online/Ki_force", parameters.Ki_force);
    ros::param::set("/robothon/online/Kd_force", parameters.Kd_force);
    ros::param::set("/robothon/online/desiredForce", parameters.desiredForce);
    ros::param::set("/robothon/online/useWiggle", parameters.useWiggle);
    ros::param::set("/robothon/online/wiggleFrequencyX", parameters.wiggleFrequencyX);
    ros::param::set("/robothon/online/wiggleFrequencyY", parameters.wiggleFrequencyY);
    ros::param::set("/robothon/online/wiggleAmplitudeX", parameters.wiggleAmplitudeX);
    ros::param::set("/robothon/online/wiggleAmplitudeY", parameters.wiggleAmplitudeY);
    if (!m_controllerManager.switchController("robothon/parameterizable_compliant_assembly_controller")) {
        ROS_ERROR("MotionPlanner.cpp: Unable to switch to Parameterizable Compliant Assembly Controller!");
        return false;
    }
    return true;
}
} // namespace Robothon::Planner
