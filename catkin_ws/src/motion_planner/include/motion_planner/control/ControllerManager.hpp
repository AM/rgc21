/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include <ros/node_handle.h>
#include <string>
#include <vector>

namespace Robothon::Planner {
//! Controller manager class.
/*!
 * The ControllerManager is responsible for switching between the controllers. E.g. an assembly operation needs a
 * different controller than a Point-to-Point motion requires. This class communicates to the
 * <ahref="http://wiki.ros.org/controller_manager">controller_manager</a> to switch between different controllers.
 *
 */
class ControllerManager {
public:
    //! Special Constructor.
    /*!
     * Start the client to swich controllers (see \ref m_switchControllerClient) and reads out all custom controllers
     * from the ROS Parameter Server that were loaded for the application.
     *
     * @param nh Handle to the ROS node in which the servers are instantiated.
     */
    explicit ControllerManager(ros::NodeHandle& nh);

    //! Activate, i.e. load a different controller.
    /*!
     * Stops the currently running custom controllers and activates the desired one. Only switches between custom
     * controllers. There are some default controllers, e.g. FrankaStateController, that are not taken care of by this
     * function.
     *
     * @param controllerName Name of the controller to be activated.
     * @return Success flag.
     */
    bool switchController(const std::string& controllerName);

private:
    //! ROS service client to switch between ROS controllers for different tasks.
    ros::ServiceClient m_switchControllerClient;

    //! Custom controllers that were loaded for the application.
    std::vector<std::string> m_customControllers;
};
} // namespace Robothon::Planner
