// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
// Source code was taken from: https://github.com/frankaemika/icra18-fci-tutorial/tree/master/icra18/src
// Source code was modified by Wittmann 2021: see comments on changes in the detailed class description.

/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include "motion_planner/log/Logger.h"
#include "motion_planner/utils/MotionPlannerConfig.hpp"
#include "motion_planner/utils/SecOrderLowPass.hpp"
#include <Eigen/Dense>
#include <controller_interface/multi_interface_controller.h>
#include <ctime>
#include <franka_hw/franka_model_interface.h>
#include <franka_hw/franka_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <memory>
#include <ros/node_handle.h>
#include <string>
#include <vector>

namespace Robothon::Planner {

//! Parameterizable Compliant Assembly Controller composed of Cartesian impedance and PID force control.
/*!
 * The controller performs an insertion or tactile interaction task with the robot's end effector. An additional
 * spiral search feature, i.e. a wiggle motion, helps to compensate position inaccuracies.
 *
 * The controller is minor modification of the
 * <a href="https://github.com/frankaemika/icra18-fci-tutorial">icra18-fci-tutorial</a> which itself is based on the
 * examples of the ROS package <a href="http://wiki.ros.org/franka_example_controllers">franka_example_controllers</a>.
 * It adds logging capabilities as well as a D-gain to the force control using a second order low pass filter. Further,
 * force control is enabled in all direction. Additionally, torque control is implemented. An I-gain is added to the
 * Cartesian impedance control law.
 *
 * The ParameterizableCompliantAssemblyController claims the following hardware interfaces:
 *  * franka_hw::FrankaModelInterface
 *  * franka_hw::FrankaStateInterface
 *  * hardware_interface::EffortJointInterface
 *
 * Within <em>ros control</em> custom controllers can be implemented if the existing ones are not sufficient (see
 * <a href="http://wiki.ros.org/ros_controllers">here</a>). To expose custom controllers to the <em>controller
 * manager</em> they have to be implemented as plugins using the <a href="http://wiki.ros.org/pluginlib">pluginlib</a>.
 * A <em>controller</em> in ROS generally consists of the functions <em>init()</em>, <em>starting()</em>,
 * <em>stopping()</em> and <em>update()</em>. The latter gets called periodically within the robot's control thread. A
 * good explanation on the former three is given
 * <a href="https://answers.ros.org/question/119316/using-controller-manager-and-getting-it-to-work/">here</a>.
 * A tutorial that shows how to implement custom controllers specifically for the <em>FRANKA EMIKA Panda</em> robot can
 * be found <a href="https://frankaemika.github.io/docs/franka_ros.html">pluginlib</a>.
 *
 * Within the Robothon project, the custom controllers are implemented as part of the <em>motion_planner</em> package.
 * However, the controllers are built within their own library <em>wallE20controllers</em> and are not part of the
 * <em>motion_planner_library</em>.
 */
class ParameterizableCompliantAssemblyController
    : public controller_interface::MultiInterfaceController<franka_hw::FrankaModelInterface,
          hardware_interface::EffortJointInterface, franka_hw::FrankaStateInterface> {
public:
    //! Mandatory initialization function that is called when the controller is loaded.
    /*!
     * Declares the handles, interfaces and limits of the hardware resources that are claimed by the controller.
     * The hardware abstraction provides the <em>ROS control</em> interfaces. Handles to these interfaces are
     * instantiated in the init() function.
     *
     * @param robot Hardware abstraction of the robot.
     * @param nodeHandle Handle to the node spinning the controllers.
     * @return Success flag.
     */
    bool init(hardware_interface::RobotHW* robot, ros::NodeHandle& nodeHandle) override;

    //! Optional function that gets executed before the first update.
    /*!
     * A typical policy is semantic zero to set the robot in a default state. Sets the robot related values according
     * to the sensor measurements (\ref m_initialPosition etc.). Further, the parameter settings for the control cycle
     * are read from the ROS parameter server (\ref m_Kp_imp etc.).
     *
     * @param time Current time.
     */
    void starting(const ros::Time& time) override;

    //! Mandatory update function that gets called within each control cycle.
    /*!
     * Is called within the <em>ROS control</em> node according to:
     *   while (true)
     *   {
     *       robot.read();
     *       cm.update(robot.get_time(), robot.get_period());
     *       robot.write();
     *       sleep();
     *   }
     *
     * @param time Current ROS time.
     * @param period Passed time since last update call.
     */
    void update(const ros::Time& time, const ros::Duration& period) override;

private:
    //! Logger for real-time data.
    std::unique_ptr<Logger> m_logger;

    //! Handle to the franka::Model loaded from the FCI.
    std::unique_ptr<franka_hw::FrankaModelHandle> m_robotModelHandle;

    //! Handle to the franka::RobotState to get the sensor measurements etc.
    std::unique_ptr<franka_hw::FrankaStateHandle> m_robotStateHandle;

    //! Handle to the joint to command the torque values.
    std::vector<hardware_interface::JointHandle> m_jointHandles;

    //! Position of the end effector before the control cycle.
    /*!
     * Used for Cartesian impedance control throughout the push process.
     */
    Eigen::Vector3d m_initialPosition;

    //! Orientation of the end effector before the control cycle.
    /*!
     * Used for Cartesian impedance control throughout the push process.
     */
    Eigen::Quaterniond m_initialOrientation;

    //! External force and torque acting on the end effector before the control cycle.
    /*!
     * Used for compensation during force control.
     */
    Eigen::Matrix<double, 6, 1> m_initialExternalForce;

    //! Integrated force error over the time the controller is running.
    Eigen::Matrix<double, 6, 1> m_intForceError;

    //! Current deviation of the desired push force.
    Eigen::Matrix<double, 6, 1> m_forceError;

    //! Integrated impedance error over the time the controller is running.
    Eigen::Matrix<double, 6, 1> m_intImpedanceError;

    //! Current deviation of the desired impedance.
    Eigen::Matrix<double, 6, 1> m_impedanceError;

    //! Derivative of the force error.
    Eigen::Matrix<double, 6, 1> m_dForceError;

    //! Derivative of the force error.
    Eigen::Matrix<double, 6, 1> m_filteredForceError;

    //! Second order low pass filter for the force error.
    std::unique_ptr<SecOrderLowPass<Eigen::Matrix<double, 6, 1>>> m_secOrderLowPassForceError;

    //! Stiffness parameters for the Cartesian impedance.
    Eigen::DiagonalMatrix<double, 6> m_Kp_imp;

    //! Stiffness parameters for the Cartesian impedance.
    Eigen::DiagonalMatrix<double, 6> m_Kd_imp;

    //! I Gains for the Cartesian impedance.
    Eigen::DiagonalMatrix<double, 6> m_Ki_imp;

    //! P gain of the PID force control.
    double m_Kp_force;

    //! I gain of the PID force control.
    double m_Ki_force;

    //! D gain of the PID force control.
    double m_Kd_force;

    //! Desired force to be applied to end effector.
    Eigen::Matrix<double, 6, 1> m_desiredForce;

    //! Use the wiggle feature.
    int m_useWiggle;

    //! See MotionPlannerConfig.
    double m_wiggleFrequencyX;

    //! See MotionPlannerConfig.
    double m_wiggleFrequencyY;

    //! See MotionPlannerConfig.
    double m_wiggleAmplitudeX;

    //! See MotionPlannerConfig.
    double m_wiggleAmplitudeY;

    //! Passed time in [s] since the controller was started.
    double m_currentTime{ 0.0 };

    //! Perform a wiggle motion of the end effector around its \ref m_initialOrientation.
    /*!
     * Performs wiggle motion as implemented in the
     * <a href="https://github.com/frankaemika/icra18-fci-tutorial">icra18-fci-tutorial</a>. The \ref
     * m_initialOrientation gets reorientated based on the passed time since the controller was started, i.e. \ref
     * m_currentTime;
     *
     * @return New orientation.
     */
    Eigen::Quaterniond wiggleOrientation() const;
};
} // namespace Robothon::Planner
