/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

namespace Robothon::Planner {

/*! Second order digital low-pass filter (PT2) for custom data type */
template <typename T> class SecOrderLowPass {

public:
    /*! Constructor
        \param _initialValue Initial value or template object
    */
    explicit SecOrderLowPass(const T& initialValue)
        : m_x(initialValue)
        , m_x1(initialValue)
        , m_x2(initialValue)
        , m_u1(initialValue)
        , m_u2(initialValue)
    {
    }

    /*! (Re) initialize the filter parameters
        \param _dt sample time
        \param _T time constant of the filter (set to zero to deactivate filter)
        \param _d damping parameter (0.0 to 1.0)
    */
    void init(const double& dt, const double& t, const double& d = 1.0)
    {
        m_T = t;
        m_dt = dt;
        m_d = d;

        if (m_T == 0) {
            // passthru
            m_a0 = 1.0;
            m_a1 = 0;
            m_a2 = 0;
            m_a3 = 1.0;
            m_a4 = 0.0;
            m_a5 = 0.0;
        } else {

            // Stability margin
            if (m_T < m_dt / 2) {
                // ToDo Implement Warning!
                // Time constant lower than half of sample time, saturating value.
                m_T = m_dt / 2;
            }

            // calculate filter parameters
            m_a0 = 4 * m_T * m_T + 4 * m_d * m_T * m_dt + m_dt * m_dt;
            m_a1 = -2 * m_dt * m_dt + 8 * m_T * m_T;
            m_a2 = -4 * m_T * m_T + 4 * m_d * m_T * m_dt - m_dt * m_dt;
            m_a3 = m_dt * m_dt;
            m_a4 = 2 * m_dt * m_dt;
            m_a5 = m_a3;
        }
    }

    /*! Reset all filter states to given value
        \param _value filter value
    */
    void reset(const T& value)
    {
        m_x2 = value;
        m_x1 = value;
        m_x = value;
        m_u1 = value;
        m_u2 = value;
    }

    /*! Reset filter value to given value and signal rate
        \param _value filter value
        \param _rate filter signal rate
    */
    void reset(const T& value, const T& rate)
    {
        m_x = value;
        m_x1 = m_x - rate * m_dt;
        m_x2 = m_x1;
        m_u1 = value;
        m_u2 = value;
    }

    /*! Compute and return the filter output
        \param _input Filter input value
    */
    const T& process(const T& input)
    {
        m_x2 = m_x1;
        m_x1 = m_x;

        // update filter output
        m_x = (m_x1 * m_a1 + m_x2 * m_a2 + input * m_a3 + m_u1 * m_a4 + m_u2 * m_a5) * (1.0 / m_a0);

        m_u2 = m_u1;
        m_u1 = input;

        return m_x;
    }

    //! Return the current filter output
    const T& getOutput() { return m_x; }

    //! Return the current derivative of the filter output
    T getDerivative() { return (m_x - m_x1) * (1.0 / m_dt); }

    //! Returns the second derivative of the filter output
    T getSecondDerivative() { return (m_x - 2.0 * m_x1 + m_x2) * (1.0 / m_dt / m_dt); }

private:
    //! state
    T m_x;

    //! old state
    T m_x1;

    //! older state
    T m_x2;

    //! old u
    T m_u1;

    //! older u
    T m_u2;

    //! sample time
    double m_dt = 0.0;

    //! time constant
    double m_T = 0.0;

    //! damping
    double m_d = 1.0;

    //! filter coefficient denominator
    double m_a0 = 1.0;

    //! filter coefficient for y[k-1]
    double m_a1 = 0.0;

    //! filter coefficient for y[k-2]
    double m_a2 = 0.0;

    //! filter coefficient for u[k]
    double m_a3 = 0.0;

    //! filter coefficient for u[k-1]
    double m_a4 = 0.0;

    //! filter coefficient for u[k-2]
    double m_a5 = 0.0;
};
} // namespace Robothon::Planner
