/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include "motion_planner/MoveJointSpace.h"
#include "motion_planner/MoveTaskSpace.h"
#include <Eigen/Geometry>
#include <geometry_msgs/Pose.h>

namespace Robothon::Planner {
//! Converts an Eigen pose to the corresponding ROS message type.
/*!
 * @param position x-y-z position of the end effector.
 * @param orientation End effector orientation formulated as quaternion.
 * @return ROS message type defining the end effector pose.
 */
geometry_msgs::Pose getRosPose(const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation);

//! Converts an Eigen pose to the corresponding ROS message type.
/*!
 * @param pose Desired pose of the end effector.
 * @return ROS message type defining the end effector pose.
 */
geometry_msgs::Pose getRosPose(const Eigen::Isometry3d& pose);

//! Formulates a move to task space goal service based on a provided Eigen pose.
/*!
 * @param position x-y-z position of the end effector.
 * @param orientation End effector orientation formulated as quaternion.
 * @return Service that may be used by a service client to call the corresponding server.
 */
motion_planner::MoveTaskSpace getMoveTaskSpaceService(
    const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation);

//! Formulates a move to task space goal service based on a provided Eigen pose.
/*!
 * @param pose Desired pose of the end effector.
 * @return Service that may be used by a service client to call the corresponding server.
 */
motion_planner::MoveTaskSpace getMoveTaskSpaceService(const Eigen::Isometry3d& pose);

//! Formulates a move to joint space goal service based on provided joint angles.
/*!
 * @param configuration Desired joint angle values in [rad].
 * @return Service that may be used by a service client to call the corresponding server.
 */
motion_planner::MoveJointSpace getMoveJointSpaceService(const std::array<double, 7>& configuration);

//! Converts a ROS pose to an Eigen position and orientation.
/*!
 * @param pose ROS pose.
 * @return Converted Eigen pose as a position vector (x,y,z) and a quaternion.
 */
std::tuple<Eigen::Vector3d, Eigen::Quaterniond> getEigenPose(geometry_msgs::Pose pose);

//! Convert position and orientation to a homogeneous transformation matrix.
Eigen::Isometry3d posRot2mat(const Eigen::Vector3d& position, const Eigen::Quaterniond& orientation);

//! Convert a homogeneous transformation matrix to position and orientation.
std::pair<Eigen::Vector3d, Eigen::Quaterniond> mat2posRot(const Eigen::Isometry3d& matrix);

//! Transforms poses defined for the TCP w.r.t. the base into poses for panda_link8, i.e. the flange,  w.r.t. the base.
/*!
 * The desired motion of the robot is often defined by defined poses of the TCP w.r.t. the robot base. However, the
 * planning modules plan motions for the panda_link8 frame, i.e. the flange frame. Therefore, the calculation in
 * between those frames is required in the modules.
 *
 * \param [in] tcpPose Pose of the TCP w.r.t. the robot base.
 * \return Pose of the flange w.r.t. robot base.
 */
Eigen::Isometry3d tcpToFlangeTransform(const Eigen::Isometry3d& tcpPoseInBase);

//! Transforms the stiffness vector.
/*!
 * Transforms the stiffness vector w.r.t. taskboard frame to the base frame and returns the reoriented stiffness
 * std::vector.
 * @param KpImpInBoardRef
 * @param boardRefInBase
 * @return Reoriented stiffness vector w.r.t. base frame.
 */
std::vector<double> transformKpImp(std::vector<double>& kpImpInBoardRef, const Eigen::Isometry3d& boardRefInBase);

//! Transforms the desired force vector.
/*!
 * Transforms the desired force vector w.r.t. the taskboard frame to base frame and returns the reoriented desired
 * force as std::vector.
 *
 * @param forceInBoardRef
 * @param boardRefInBase
 * @return Reoriented desired force vector w.r.t. base frame.
 */
std::vector<double> transformForce(std::vector<double>& forceInBoardRef, const Eigen::Isometry3d& boardRefInBase);

//! Performs a scalar transformation.
/*!
 * Rotates params around a certain angle and returns the absolute values.
 * @param wiggleX
 * @param wiggleY
 * @param zRotationAngle
 * @return Absolute values of rotated wiggle values as tuple.
 */
std::tuple<double, double> transformWiggle(double wiggleX, double wiggleY, double zRotationAngle);
} // namespace Robothon::Planner