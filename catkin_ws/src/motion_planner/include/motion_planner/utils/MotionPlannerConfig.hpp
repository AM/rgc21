/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include <Eigen/Dense>
#include <ros/node_handle.h>
#include <ros/ros.h>
#include <string>

namespace Robothon::Planner {
//! Global configuration class for the motion planner.
/*!
 * The configuration class is implemented according to the Meyers' singleton principle. Only one object is created
 * during the runtime of the program.
 * The configuration parameters are kept constant but they are read out from the ROS parameter server at program
 * start during their initialization. Therefore, the getValueFromPlannerYaml(...) template function is implemented.
 */
class MotionPlannerConfig {
public:
    //! Getter for the static MotionPlannerConfig object.
    static MotionPlannerConfig& get()
    {
        static MotionPlannerConfig instance;
        return instance;
    }

private:
    //! Specialized constructor.
    MotionPlannerConfig() = default;
    //! Use the default destructor.
    ~MotionPlannerConfig() = default;
    //! Delete the copy constructor (singleton).
    MotionPlannerConfig(const MotionPlannerConfig&) = delete;
    //! Delete the copy assignment operator (singleton).
    MotionPlannerConfig& operator=(const MotionPlannerConfig&) = delete;

    //! Template function to load values from the parameter server within the planner namespace.
    template <typename T> T getValueFromPlannerYaml(const std::string& variableName)
    {
        T variable;
        std::string fullVariableName = "/robothon/motion_planner_config/" + variableName;
        if (!ros::param::get(fullVariableName, variable)) {
            ROS_ERROR_STREAM("Failed to load parameter: " << fullVariableName);
        }
        return variable;
    }

public:
    //! Home configuration of the robot in [rad].
    const Eigen::Matrix<double, 7, 1> m_homeConfiguration = [&]() {
        auto home = getValueFromPlannerYaml<std::vector<double>>("homeConfiguration");
        return Eigen::Map<Eigen::Matrix<double, 7, 1>>(home.data());
    }();
    //! Additional allowed collision pairs besides those mentioned in the panda_moveit_config files.
    const std::vector<std::pair<std::string, std::string>> m_allowedCollisionPairs = [&]() {
        auto configuredCollisionPairs = getValueFromPlannerYaml<XmlRpc::XmlRpcValue>("allowedCollisionPairs");
        std::vector<std::pair<std::string, std::string>> allowedCollisionPairsVector;
        for (int ii = 0; ii < configuredCollisionPairs.size(); ++ii) {
            allowedCollisionPairsVector.emplace_back(
                std::make_pair(std::string(configuredCollisionPairs[ii]["element1"]),
                    std::string(configuredCollisionPairs[ii]["element2"])));
        }
        return allowedCollisionPairsVector;
    }();
    //! Additional allowed collision elements besides those mentioned in the panda_moveit_config files.
    const std::vector<std::string> m_allowedCollisionElement{ getValueFromPlannerYaml<std::vector<std::string>>(
        "allowedCollisionElement") };
};
} // namespace Robothon::Planner
