/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include <Eigen/Geometry>

//! Stores the forces and positions of the end effector.
/*!
 * E.g. used by the TaskSupervisor to determine task success.
 */
struct EndEffectorState {
    Eigen::Matrix<double, 6, 1> m_externalWrenchKFrame; // External wrench on TCP w.r.t. the K frame.
    Eigen::Matrix<double, 6, 1> m_externalWrenchBaseFrame; // External wrench on TCP w.r.t. the robot's base frame.
    Eigen::Vector3d m_eePosition; // Position of the TCP w.r.t. the robot base.
    Eigen::Matrix<double, 3, 3> m_eeRotation; // Rotation of the TCP frame w.r.t. the robot base.
};