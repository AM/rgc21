/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include <array>
#include <chrono>
#include <cstddef>
#include <fstream>
#include <iostream>

namespace Robothon::Planner {
//! Logger class.
/*!
 * Used in ROS controllers for real-time logging and controller debugging and in the MotionPlanner.
 */
class Logger {
public:
    //! Constructor, which needs a string for the filename.
    explicit Logger(std::string);

    //! Filename/Filepath.
    std::string m_fileName;

    //! Handle for the repeatedly used logfile.
    std::ofstream m_logFileHandle;

    //! Stores the time, when the Logger object was instantiated.
    std::chrono::high_resolution_clock::time_point m_startTime;

    //! Opens the logfile.
    /*!
     * The name of the logfile has to be defined in the constructor of a Robothon::Planner::Logger object.
     *
     * @param header Header to write on top of the logfile. Can be useful if file is read as Dataframe e.g. by Python.
     */
    void openLogfile(const std::string& header);

    //! Prints data to the logfile.
    /*!
     * The logfile name has to be defined in the constructor of a Robothon::Planner::Logger object.
     *
     * @param logData Fixed-sized array, which will be stored in a new row of the logfile.
     */
    template <std::size_t SIZE> void writeToLogfile(const std::array<double, SIZE>& logData);
};
} // namespace Robothon::Planner

//! Prints the time and then the array in one row.
template <std::size_t SIZE> void Robothon::Planner::Logger::writeToLogfile(const std::array<double, SIZE>& logData)
{
    m_logFileHandle
        << std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - m_startTime).count()
            / 1000
        << ";";
    for (const auto& el : logData) {
        m_logFileHandle << el << ";";
    }
    m_logFileHandle << "\n";
}