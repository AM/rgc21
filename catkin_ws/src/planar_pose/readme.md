## Planar Pose Estimation
This package is derived from: "Object Detection and Pose Estimation from RGB and Depth Data for Real-time, Adaptive Robotic Grasping", Paul et al., IPCV 2020 (https://github.com/paul-shuvo/planar_pose).
Please consider citing this work when using the package (see below).
The code of this package is released under the MIT license (see below).

[![License: MIT](https://img.shields.io/badge/license-MIT-blue)](https://opensource.org/licenses/MIT)


### Run

This package contains a object detection module and a planar pose estimation module. Planar pose estimation module depends on the object detection module.

To run the object detection module:
```
cd 'your_project_path`
source devel/setup.bash
rosrun planar_pose object_detection.py
```

Then, to run the planar pose estimation module: 
```
cd 'your_project_path`
source devel/setup.bash
rosrun planar_pose planar_pose_estimation.py
```
### Citation
Derived from:

```tex
@inproceedings{paul2020object,
      title={Object Detection and Pose Estimation from RGB and Depth Data for Real-time, Adaptive Robotic Grasping}, 
      author={S. K. Paul and M. T. Chowdhury and M. Nicolescu and M. Nicolescu},
      booktitle={Proceedings of the International Conference on Image Processing, Computer Vision, and Pattern Recognition (IPCV)}
      year={2020},    
}
```