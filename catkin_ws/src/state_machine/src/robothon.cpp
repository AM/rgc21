/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

//! Solves the five tasks on the board as part of the Robothon Grand Challenge 2021.

#include <Eigen/Geometry>
#include <actionlib/client/simple_action_client.h>
#include <chrono>
#include <franka_gripper/GraspAction.h>
#include <franka_gripper/MoveAction.h>
#include <franka_msgs/SetForceTorqueCollisionBehavior.h>
#include <motion_planner/AddObject.h>
#include <motion_planner/GetLinkEightFrameInBase.h>
#include <motion_planner/MoveJointSpace.h>
#include <motion_planner/MoveTaskSpace.h>
#include <motion_planner/utils/utility_functions.hpp>
#include <ros/node_handle.h>
#include <state_machine/task_board_detector/TaskBoardDetector.hpp>
#include <state_machine/utils/utility_functions.hpp>

//! Main execution function.
int main(int argc, char* argv[])
{
    // Hard coded values.
    const std::array<double, 7> homeConfiguration{ 0.0, 0.0, 0.0, -1.57, 0.0, 1.57, 0.7853 };
    const std::array<double, 7> boxDetectionConfiguration{ 0.085, 0.23, 0.07, -1.72, -0.09, 2.00, 0.93 };
    const Eigen::Isometry3d boardOriginInBoardRef = Robothon::Planner::posRot2mat(
        Eigen::Vector3d(0.131 / 2.0, 0.117, -0.08), Eigen::Quaterniond(0.707, 0.0, 0.0, 0.707).normalized());
    const double offsetFingers = 0.035; // z-Offset of the 3D-printed finger tips to the end effector frame in [m].

    // Check for the argument of the vision usage.
    bool useVision = std::string{ argv[1] } == "true";

    // Initialize the ROS node.
    ros::init(argc, argv, "robothon_node");
    ros::NodeHandle nh;

    // Required ROS service clients.
    ros::ServiceClient moveJointSpaceClient = nh.serviceClient<motion_planner::MoveJointSpace>("moveJointSpace");
    ros::ServiceClient moveTaskSpaceClient = nh.serviceClient<motion_planner::MoveTaskSpace>("moveTaskSpace");
    ros::ServiceClient PCACClient = nh.serviceClient<motion_planner::ParameterizableCompliantAssemblyControl>(
        "parameterizableCompliantAssemblyControl");
    ros::ServiceClient addObjectClient = nh.serviceClient<motion_planner::AddObject>("addObject");
    ros::ServiceClient getLinkEightFrameInBaseClient
        = nh.serviceClient<motion_planner::GetLinkEightFrameInBase>("getLinkEightFrameInBase");
    actionlib::SimpleActionClient<franka_gripper::MoveAction> moveGripperClient{ "franka_gripper/move", true };
    actionlib::SimpleActionClient<franka_gripper::GraspAction> graspClient{ "franka_gripper/grasp", true };
    ros::ServiceClient setCollisionBehaviorClient = nh.serviceClient<franka_msgs::SetForceTorqueCollisionBehavior>(
        "franka_control/set_force_torque_collision_behavior");
    PCACClient.waitForExistence();
    moveJointSpaceClient.waitForExistence();
    moveTaskSpaceClient.waitForExistence();
    addObjectClient.waitForExistence();
    moveGripperClient.waitForServer();
    graspClient.waitForServer();
    setCollisionBehaviorClient.waitForExistence();
    getLinkEightFrameInBaseClient.waitForExistence();

    // Set proper collision detection thresholds for the robot.
    franka_msgs::SetForceTorqueCollisionBehavior collisionBehaviorGoal;
    collisionBehaviorGoal.request.lower_torque_thresholds_nominal = { 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0 };
    collisionBehaviorGoal.request.upper_torque_thresholds_nominal = { 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0 };
    collisionBehaviorGoal.request.lower_force_thresholds_nominal = { 10.0, 10.0, 10.0, 10.0, 10.0, 10.0 };
    collisionBehaviorGoal.request.upper_force_thresholds_nominal = { 100.0, 100.0, 100.0, 100.0, 100.0, 100.0 };
    setCollisionBehaviorClient.call(collisionBehaviorGoal);

    // Home the robot and empty the gripper.
    ROS_INFO("Home the robot.");
    auto home = Robothon::Planner::getMoveJointSpaceService(homeConfiguration);
    moveJointSpaceClient.call(home);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.02, 0.1);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.0, 0.1);

    // Add the static environment.
    ROS_INFO("Add laboratory environment");
    motion_planner::AddObject laboratory;
    laboratory.request.id = 0; // See Robothon::Planner::StaticObjectType.
    laboratory.request.pose = Robothon::Planner::getRosPose(
        (Eigen::Vector3d() << 0.0, 0.0, 0.0).finished(), Eigen::Quaterniond(1.0, 0.0, 0.0, 0.0));
    addObjectClient.call(laboratory);

    // Get the board reference frame for the planner.
    Eigen::Isometry3d boardPlannerRefInBase = Robothon::Planner::posRot2mat(
        Eigen::Vector3d(0.475, -0.042, -0.05), Eigen::Quaterniond(1.0, 0.0, 0.0, 0.0).normalized()); // Default.
    if (useVision) {
        auto boxDetection = Robothon::Planner::getMoveJointSpaceService(boxDetectionConfiguration);
        moveJointSpaceClient.call(boxDetection);
        Robothon::StateMachine::TaskBoardDetector taskBoardDetector;
        boardPlannerRefInBase = taskBoardDetector.getVisionIso3dPose();
        // Make some adjustments to the determined pose by the vision module.
        // Use a vertical z-orientation of the task board.
        Eigen::Quaterniond straight(boardPlannerRefInBase.linear());
        straight.x() = 0.0;
        straight.y() = 0.0;
        straight.normalize();
        boardPlannerRefInBase.linear() = straight.toRotationMatrix();
        // The z-height of the task board is known by the setup. Change according to yours.
        boardPlannerRefInBase.translation()[2] = -0.05;
    }
    // Add the detected task board to the environment.
    motion_planner::AddObject taskBoard;
    taskBoard.request.id = 1; // See Robothon::Planner::StaticObjectType.
    taskBoard.request.pose = Robothon::Planner::getRosPose(boardPlannerRefInBase * boardOriginInBoardRef);
    addObjectClient.call(taskBoard);

    // Callable to move to the tasks.
    auto moveToTask = [&](const std::string& taskName) {
        auto [position, orientation] = Robothon::State_Machine::getTaskCoordinate(taskName, offsetFingers);
        if (!Robothon::State_Machine::moveToTcpPose(
                moveTaskSpaceClient, boardPlannerRefInBase, position, orientation)) {
            ROS_ERROR_STREAM("Failed to move to task " << taskName);
        }
    };

    // Callable to execute the PCAC for a task.
    motion_planner::ParameterizableCompliantAssemblyControl parameterizableCompliantAssemblyControl;
    auto pcac = [&](const std::string& taskName, int taskID) {
        parameterizableCompliantAssemblyControl
            = Robothon::State_Machine::getTaskParameterization(taskName, boardPlannerRefInBase);
        parameterizableCompliantAssemblyControl.request.taskID = taskID;
        PCACClient.call(parameterizableCompliantAssemblyControl);
        if (!parameterizableCompliantAssemblyControl.response.success) {
            ROS_ERROR_STREAM(taskName << " failed.");
        }
    };

    // ----------------------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------------------

    // ------ Blue Button -------
    moveToTask("blue_button");
    ROS_INFO("Push the blue button.");
    pcac("button", 0);
    moveToTask("blue_button");
    moveJointSpaceClient.call(home);
    // ------ Insert Key --------
    moveToTask("pre_key_remove");
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.02, 0.1);
    moveToTask("key_remove");
    Robothon::State_Machine::grasp(graspClient, 0.005, 0.05, 80.0);
    moveToTask("pre_key_remove");
    moveToTask("key_insert");
    ROS_INFO("Insert the Key.");
    parameterizableCompliantAssemblyControl
        = Robothon::State_Machine::getTaskParameterization("insert_key", boardPlannerRefInBase);
    parameterizableCompliantAssemblyControl.request.taskID = 1;
    auto [positionKey, orientationKey] = Robothon::State_Machine::getTaskCoordinate("key_insert", offsetFingers);
    if (!Robothon::State_Machine::errorHandling(PCACClient, parameterizableCompliantAssemblyControl, positionKey,
            orientationKey, boardPlannerRefInBase, moveTaskSpaceClient)) {
        ROS_ERROR("Failed to insert the key.");
    }
    // ------ Turn Key --------
    ROS_INFO("Turn the Key.");
    pcac("turn_key", 2);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.04, 0.1);
    moveToTask("key_insert");
    moveJointSpaceClient.call(home);
    // -------- Pick LAN --------
    moveToTask("pre_lan_pick");
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.03, 0.1);
    moveToTask("lan_pick");
    Robothon::State_Machine::grasp(graspClient, 0.005, 0.05, 25.0);
    pcac("pick_lan", 3);
    // ------ Insert LAN --------
    ROS_INFO("Insert LAN.");
    moveToTask("lan_insert");
    parameterizableCompliantAssemblyControl
        = Robothon::State_Machine::getTaskParameterization("insert_lan", boardPlannerRefInBase);
    parameterizableCompliantAssemblyControl.request.taskID = 4;
    auto [positionLAN, orientationLAN] = Robothon::State_Machine::getTaskCoordinate("lan_insert", offsetFingers);
    if (!Robothon::State_Machine::errorHandling(PCACClient, parameterizableCompliantAssemblyControl, positionLAN,
            orientationLAN, boardPlannerRefInBase, moveTaskSpaceClient)) {
        ROS_ERROR("Failed to insert the LAN cable.");
    }
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.03, 0.1);
    moveToTask("post_lan_insert");
    moveJointSpaceClient.call(home);
    // -------- Open Lid --------
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.000, 0.1);
    moveToTask("lid_open");
    ROS_INFO("Open Lid");
    pcac("open_lid", 5);
    // -------- Remove Lid ------
    moveToTask("pre_lid_remove");
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.05, 0.05);
    moveToTask("lid_remove");
    Robothon::State_Machine::grasp(graspClient, 0.033, 0.04, 80.0);
    ROS_INFO("Remove Lid.");
    pcac("remove_lid", 6);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.05, 0.05);
    // --- Remove Battery 2 -----
    moveToTask("pre_battery_2_remove");
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.06, 0.05);
    moveToTask("battery_2_remove");
    Robothon::State_Machine::grasp(graspClient, 0.048, 0.005, 80.0);
    ROS_INFO("Remove Battery 2.");
    pcac("remove_battery_2", 7);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.06, 0.05);
    moveToTask("post_battery_2_remove");
    moveToTask("battery_2_pick");
    Robothon::State_Machine::grasp(graspClient, 0.012, 0.05, 40.0);
    moveToTask("post_battery_2_pick");
    // --- Insert Battery 2 -----
    moveToTask("pre_battery_2_insert");
    ROS_INFO("Insert Battery 2.");
    pcac("insert_battery", 8);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.05, 0.1);
    moveToTask("post_battery_2_insert");
    // --- Remove Battery 1 -----
    moveToTask("pre_battery_1_remove");
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.06, 0.05);
    moveToTask("battery_1_remove");
    Robothon::State_Machine::grasp(graspClient, 0.048, 0.005, 80.0);
    ROS_INFO("Remove Battery 1.");
    pcac("remove_battery_1", 9);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.06, 0.05);
    moveToTask("pre_battery_1_pick");
    moveToTask("battery_1_pick");
    Robothon::State_Machine::grasp(graspClient, 0.012, 0.05, 40.0);
    moveToTask("post_battery_1_pick");
    // --- Insert Battery 1 -----
    moveToTask("pre_pre_battery_1_insert");
    moveToTask("pre_battery_1_insert");
    ROS_INFO("Insert Battery 1.");
    pcac("insert_battery", 8);
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.05, 0.1);
    moveToTask("pre_battery_1_insert");
    moveJointSpaceClient.call(home);
    // ------- Red button -------
    Robothon::State_Machine::moveGripper(moveGripperClient, 0.0, 0.1);
    moveToTask("red_button");
    ROS_INFO("Push the red button.");
    pcac("button", 0);
    moveToTask("red_button");
    moveJointSpaceClient.call(home);
    return 0;
}
