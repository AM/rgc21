/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#include "motion_planner/utils/utility_functions.hpp"
#include "state_machine/utils/utility_functions.hpp"
#include <franka_gripper/GraspGoal.h>
#include <franka_gripper/MoveGoal.h>
#include <ros/console.h>

//! Performs a grasp action with the FRANKA hand.
bool Robothon::State_Machine::grasp(
    actionlib::SimpleActionClient<franka_gripper::GraspAction>& client, double width, double speed, double force)
{
    franka_gripper::GraspGoal graspGoal;
    graspGoal.width = width;
    graspGoal.speed = speed;
    graspGoal.force = force;
    franka_gripper::GraspEpsilon graspEpsilon;
    graspEpsilon.inner = 0.01;
    graspEpsilon.outer = 0.01;
    graspGoal.epsilon = graspEpsilon;
    client.sendGoal(graspGoal);
    client.waitForResult();
    return (client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED);
}

//! Move the fingers of the FRANKA hand.
bool Robothon::State_Machine::moveGripper(
    actionlib::SimpleActionClient<franka_gripper::MoveAction>& client, double width, double speed)
{
    franka_gripper::MoveGoal openGripperGoal;
    openGripperGoal.width = width;
    openGripperGoal.speed = speed;
    client.sendGoal(openGripperGoal);
    client.waitForResult();
    return (client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED);
}

//! Calls the motion planner to move the TCP to a desired pose w.r.t. the board reference.
bool Robothon::State_Machine::moveToTcpPose(ros::ServiceClient& client, const Eigen::Isometry3d& boardRefInBase,
    const Eigen::Matrix<double, 3, 1>& position, const Eigen::Quaterniond& orientation)
{
    Eigen::Isometry3d desiredTcpPoseInBoardRef = Robothon::Planner::posRot2mat(position, orientation);
    motion_planner::MoveTaskSpace motionCall
        = Robothon::Planner::getMoveTaskSpaceService(boardRefInBase * desiredTcpPoseInBoardRef);
    if (!client.call(motionCall)) {
        ROS_ERROR("Failed to call the MoveToTaskSpace server!");
        return false;
    }
    return motionCall.response.success;
}

//! Get the parameters for the Parameterizable Compliant Assembly Controller for a specific task.
motion_planner::ParameterizableCompliantAssemblyControl Robothon::State_Machine::getTaskParameterization(
    const std::string& taskName, const Eigen::Isometry3d& boardPlannerRefInBase)
{
    // Load the task parameters from the ROS parameter server.
    std::vector<double> kImp;
    double kP;
    double kI;
    double kD;
    std::vector<double> force;
    int useWiggle;
    double fx;
    double fy;
    double Ax;
    double Ay;
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/K_imp", kImp)) {
        ROS_ERROR("Could not read K_imp from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/k_p", kP)) {
        ROS_ERROR("Could not read k_p from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/k_i", kI)) {
        ROS_ERROR("Could not read k_i from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/k_d", kD)) {
        ROS_ERROR("Could not read k_d from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/force", force)) {
        ROS_ERROR("Could not read force from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/use_wiggle", useWiggle)) {
        ROS_ERROR("Could not read useWiggle from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/f_x", fx)) {
        ROS_ERROR("Could not read f_x from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/f_y", fy)) {
        ROS_ERROR("Could not read f_y from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/A_x", Ax)) {
        ROS_ERROR("Could not read A_x from the ROS parameter server.");
    }
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/A_y", Ay)) {
        ROS_ERROR("Could not read A_y from the ROS parameter server.");
    }
    // Generate the ROS service call with the specified parameters.
    motion_planner::ParameterizableCompliantAssemblyControl taskParameters;
    // Convert the Cartesian impedance stiffness according to the task board location.
    taskParameters.request.Kp_imp = Robothon::Planner::transformKpImp(kImp, boardPlannerRefInBase);
    taskParameters.request.Kp_force = kP;
    taskParameters.request.Ki_force = kI;
    taskParameters.request.Kd_force = kD;
    taskParameters.request.desiredForce = Robothon::Planner::transformForce(force, boardPlannerRefInBase);
    taskParameters.request.useWiggle = useWiggle;
    taskParameters.request.wiggleFrequencyX = fx;
    taskParameters.request.wiggleFrequencyY = fy;
    // Convert the spiral search amplitude according to the task board location.
    std::tie(Ax, Ay)
        = Robothon::Planner::transformWiggle(Ax, Ay, std::abs(boardPlannerRefInBase.linear().eulerAngles(0, 1, 2)[2]));
    taskParameters.request.wiggleAmplitudeX = Ax;
    taskParameters.request.wiggleAmplitudeY = Ay;

    return taskParameters;
}

//! Get the coordinates for the tasks.
std::pair<Eigen::Vector3d, Eigen::Quaterniond> Robothon::State_Machine::getTaskCoordinate(
    const std::string& taskName, double offsetFingers)
{
    std::vector<double> position;
    std::vector<double> orientation;
    if (!ros::param::get("/robothon/state_machine/" + taskName + "/position", position)
        || !ros::param::get("/robothon/state_machine/" + taskName + "/orientation", orientation)) {
        ROS_ERROR_STREAM("Could not read pose for task " << taskName << " from the ROS parameter server.");
    }
    Eigen::Matrix<double, 3, 1> eigenPosition;
    eigenPosition << position.at(0), position.at(1), position.at(2) + offsetFingers;
    Eigen::Quaterniond eigenOrientation(orientation.at(0), orientation.at(1), orientation.at(2), orientation.at(3));
    eigenOrientation.normalize();
    return std::make_pair(eigenPosition, eigenOrientation);
}

//! Error handling strategy for more involved tasks.
bool Robothon::State_Machine::errorHandling(ros::ServiceClient& pcac,
    motion_planner::ParameterizableCompliantAssemblyControl& pcacServiceCall, const Eigen::Vector3d& initialPosition,
    const Eigen::Quaterniond& insertOrientation, const Eigen::Isometry3d boardPlannerRefInBase,
    ros::ServiceClient& moveTaskSpaceClient)
{
    // Try to insert the key multiple times, if the controller doesn't finish after some seconds.
    pcac.call(pcacServiceCall);
    int maxNumOfTrials = 6;
    int numOfTrials = 0;
    double offsetRetryInsertion = 0.00045;
    while (!pcacServiceCall.response.success) {
        ROS_WARN_STREAM("Service Failed. Number of Trials: " << ++numOfTrials);
        // Reposition the end effector.
        if (!Robothon::State_Machine::moveToTcpPose(moveTaskSpaceClient, boardPlannerRefInBase,
                (Eigen::Matrix<double, 3, 1>() << initialPosition[0]
                            + ((numOfTrials % 2) ? -numOfTrials * offsetRetryInsertion
                                                 : numOfTrials * offsetRetryInsertion),
                    initialPosition[1], initialPosition[2])
                    .finished(),
                insertOrientation)) {
            ROS_ERROR("Failed to move to Pre Key Insert (Retrial phase).");
        }
        // Retry.
        pcac.call(pcacServiceCall);
        if (numOfTrials > maxNumOfTrials) {
            return false;
        }
    }
    return true;
}
