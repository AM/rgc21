/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

//! Pushes the red button on the task board.

#include <Eigen/Geometry>
#include <actionlib/client/simple_action_client.h>
#include <chrono>
#include <franka_gripper/GraspAction.h>
#include <franka_gripper/MoveAction.h>
#include <motion_planner/AddObject.h>
#include <motion_planner/MoveJointSpace.h>
#include <motion_planner/MoveTaskSpace.h>
#include <motion_planner/utils/utility_functions.hpp>
#include <ros/node_handle.h>
#include <state_machine/utils/utility_functions.hpp>
#include <thread>

//! Main execution function.
int main(int argc, char* argv[])
{
    // Check for the argument of the vision usage.
    bool useVision = std::string{ argv[1] }.compare("false");
    if (useVision) {
        ROS_ERROR_STREAM("The vision module is not available for this file! Continuing without the vision module.");
    }

    // Initialize the ROS node.
    ros::init(argc, argv, "calibrate_node");
    ros::NodeHandle nh;

    // Instantiate the required service clients and action clients for the application.
    ros::ServiceClient moveJointSpaceClient = nh.serviceClient<motion_planner::MoveJointSpace>("moveJointSpace");
    ros::ServiceClient moveTaskSpaceClient = nh.serviceClient<motion_planner::MoveTaskSpace>("moveTaskSpace");
    ros::ServiceClient addObjectClient = nh.serviceClient<motion_planner::AddObject>("addObject");
    actionlib::SimpleActionClient<franka_gripper::MoveAction> moveGripperClient{ "franka_gripper/move", true };
    actionlib::SimpleActionClient<franka_gripper::GraspAction> graspClient{ "franka_gripper/grasp", true };
    // Wait for the ROS servers for the application.
    moveJointSpaceClient.waitForExistence();
    moveTaskSpaceClient.waitForExistence();
    addObjectClient.waitForExistence();
    moveGripperClient.waitForServer();
    graspClient.waitForServer();

    // Add the static environment.
    ROS_INFO("Add laboratory environment.");
    motion_planner::AddObject laboratory;
    laboratory.request.id = 0;
    laboratory.request.pose = Robothon::Planner::getRosPose(
        (Eigen::Vector3d() << 0.0, 0.0, 0.0).finished(), Eigen::Quaterniond(1.0, 0.0, 0.0, 0.0));
    addObjectClient.call(laboratory);

    // Home the robot and close gripper.
    ROS_INFO("Home the robot.");
    auto home = Robothon::Planner::getMoveJointSpaceService({ 0.0, 0.0, 0.0, -1.57, 0.0, 1.57, 0.7853 });
    moveJointSpaceClient.call(home);

    // Close the gripper.
    ROS_INFO("Close Gripper.");
    if (Robothon::State_Machine::moveGripper(moveGripperClient, 0.0, 0.1)) {
        ROS_ERROR("calibrate.cpp: Failed to close the gripper!");
    }

    // Define the reference frame of the board w.r.t. the board's origin. The latter lies in the bottom center.
    const Eigen::Isometry3d boardOriginInBoardRef = Robothon::Planner::posRot2mat(
        Eigen::Vector3d(0.131 / 2.0, 0.117, -0.08), Eigen::Quaterniond(0.707, 0.0, 0.0, 0.707).normalized());

    // Call vision to get the task board pose...
    const Eigen::Isometry3d boardRefInBase = Robothon::Planner::posRot2mat(
        Eigen::Vector3d(0.5, 0.0, -0.03), Eigen::Quaterniond(1.0, 0.0, 0.0, 0.0).normalized());

    // Add the task board to the environment.
    motion_planner::AddObject taskBoard;
    taskBoard.request.id = 1;
    taskBoard.request.pose = Robothon::Planner::getRosPose(boardRefInBase * boardOriginInBoardRef);
    addObjectClient.call(taskBoard);

    // The offset of the finger tips to the Tcp frame.
    const double offsetFingers = 0.035;
    // --------------------------
    if (!Robothon::State_Machine::moveToTcpPose(moveTaskSpaceClient, boardRefInBase,
            (Eigen::Matrix<double, 3, 1>() << 0.0, 0.0, -0.007 + offsetFingers).finished(),
            Eigen::Quaterniond(0.0, 1.0, 0.0, 0.0).normalized())) {
        ROS_ERROR("calibrate.cpp: Failed to move to first board reference (origin of taskboard frame).");
    }
    // --------------------------
    std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    // --------------------------
    ROS_INFO("Home the robot.");
    moveJointSpaceClient.call(home);
    // --------------------------
    if (!Robothon::State_Machine::moveToTcpPose(moveTaskSpaceClient, boardRefInBase,
            (Eigen::Matrix<double, 3, 1>() << 0.0, 0.237, -0.007 + offsetFingers).finished(),
            Eigen::Quaterniond(0.0, 1.0, 0.0, 0.0).normalized())) {
        ROS_ERROR("calibrate.cpp: Failed to move to second board reference.");
    }
    // --------------------------
    std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    // --------------------------
    ROS_INFO("Home the robot.");
    moveJointSpaceClient.call(home);
    return 0;
}
