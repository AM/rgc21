/*
 * This file is part of the robothon2021 project.
 * https://gitlab.lrz.de/AM/robothon2021.git
 */

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <actionlib/client/simple_action_client.h>
#include <franka_gripper/GraspAction.h>
#include <franka_gripper/MoveAction.h>
#include <motion_planner/ParameterizableCompliantAssemblyControl.h>
#include <ros/ros.h>
#include <string>

namespace Robothon::State_Machine {
//! Performs a grasp action with the FRANKA hand.
/*!
 * @param client Simple action client used to call the grasp service.
 * @param width Estimated width of the object to be grasped [m].
 * @param speed Desired grasping velocity of the fingers [m/s].
 * @param force Desired grasping force of the fingers [N].
 * @return Success flag.
 */
bool grasp(
    actionlib::SimpleActionClient<franka_gripper::GraspAction>& client, double width, double speed, double force);

//! Move the fingers of the FRANKA hand.
/*!
 * @param client Simple action client used to call the service.
 * @param width Desired width of the fingers [m].
 * @param speed Desired velocity of the fingers' motion [m/s].
 * @return Success flag.
 */
bool moveGripper(actionlib::SimpleActionClient<franka_gripper::MoveAction>& client, double width, double speed);

//! Calls the motion planner to move the end effector to a desired pose w.r.t. the task board reference.
/*!
 * @param client Service client to call the server.
 * @param boardRefInBase Pose of the task board's reference frame w.r.t. the robot base. Used to transform the
 * provided goal that is given w.r.t. the task board reference frame.
 * @param position Desired position of the end effector w.r.t. the task board frame [m].
 * @param orientation Desired orientation of the end effector w.r.t. the task board frame.
 * @return Success flag.
 */
bool moveToTcpPose(ros::ServiceClient& client, const Eigen::Isometry3d& boardRefInBase,
    const Eigen::Matrix<double, 3, 1>& position, const Eigen::Quaterniond& orientation);

//! Get the parameters for the Parameterizable Compliant Assembly Controller for a specific task.
/*!
 * Reads the corresponding parameters from the ROS parameter server. Make sure, you loaded the yaml file. Transforms
 * stiffness matrices etc. according to the current task board pose.
 *
 * @param taskName Task name as on the ROS parameter server.
 * @param boardPlannerRefInBase Task board pose w.r.t. base.
 * @return Service call for the Parameterizable Compliant Assembly Control service provided by the motion planner.
 */
motion_planner::ParameterizableCompliantAssemblyControl getTaskParameterization(
    const std::string& taskName, const Eigen::Isometry3d& boardPlannerRefInBase);

//! Get the coordinates for the tasks.
/*!
 * Reads the corresponding task coordinates, i.e. desired end effector position and orientation, from the ROS parameter
 * server. Adds the offset of the fingers to z-coordinate.
 *
 * @param taskName Task name as on the ROS parameter server.
 * @param offsetFingers Task board pose w.r.t. base.
 * @return Desired position and orientation of the end effector.
 */
std::pair<Eigen::Vector3d, Eigen::Quaterniond> getTaskCoordinate(const std::string& taskName, double offsetFingers);

//! Error handling strategy for more involved tasks.
/*!
 * Due to inaccuracies etc. e.g. the key insertion task might fail. In case of failure several attempts might be
 * performed with slightly modified initial poses before the insertion. This fucntion supervises the execution of the
 * Parameterizable Compliant Assembly Controller and implements the error handling strategy.
 *
 * @param pcac Service client of the PCAC.
 * @param pcacServiceCall Service call Service call for the PCAC.
 * @param initialPosition Initial position of the end effector that will be modified for reattempts.
 * @param insertOrientation Initial orientation of the end effector that will be modified for reattempts.
 * @param boardPlannerRefInBase Task board pose w.r.t. base.
 * @param moveTaskSpaceClient Service client for a task space motion.
 * @return Success flag.
 */
bool errorHandling(ros::ServiceClient& pcac, motion_planner::ParameterizableCompliantAssemblyControl& pcacServiceCall,
    const Eigen::Vector3d& initialPosition, const Eigen::Quaterniond& insertOrientation,
    const Eigen::Isometry3d boardPlannerRefInBase, ros::ServiceClient& moveTaskSpaceClient);
} // namespace Robothon::State_Machine
