## Environment & System Configuration
The project is developed and tested with the following configuration:
* Ubuntu 18.04
* Kernel: 5.4.102-rt53
* gcc/g++ 7.5.0
* cmake 3.10.2
* C++17

## Build
1. Install *ROS Melodic* as described [here](http://wiki.ros.org/melodic/Installation/Ubuntu).
1. Make sure you have the most up to date packages:
```bash
rosdep update
sudo apt-get update
sudo apt-get dist-upgrade
```
1. For the later *MoveIt!* source build, the following tools are required:
` sudo apt install python-wstool python-catkin-tools clang-format-10 python-rosdep `
1. Install *catkin*: `sudo apt-get install ros-melodic-catkin python-catkin-tools`
1. Install *Eigen*: `sudo apt install libeigen3-dev`
1. Install *libfranka*: `sudo apt install ros-melodic-libfranka`
1. Install `clang` tools: `sudo apt install clang-tools-6.0 clang-format-6.0 clang-tidy**-6.0`
1. Some controllers might be missing, thus run: `sudo apt-get install ros-melodic-ros-control ros-melodic-ros-controllers`
1. Clone the project with all its submodules: `git clone --recursive https://gitlab.lrz.de/AM/robothon2021.git`
1. Fetch submodules:
   ```bash
   git submodule init
   git submodule update
   ```
1. Configure the *catkin* workspace and build the project:
    ```bash
    cd robothon2021/catkin_ws
    source /opt/ros/melodic/setup.bash
    rosdep install -y --from-paths src --ignore-src --rosdistro ${ROS_DISTRO}
    catkin config --extend /opt/ros/${ROS_DISTRO} --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release
    catkin build
    ```
1. Now, you should be able to run the *MoveIt!* tutorials in simulation, see [here](http://docs.ros.org/en/melodic/api/moveit_tutorials/html/doc/quickstart_in_rviz/quickstart_in_rviz_tutorial.html#).

## Documentation
1. Install doxygen via `apt install doxygen`.
1. Building and opening the html documentation e.g. with firefox:
```bash
cd <path_to_this_project>/doc/
doxygen Doxyfile        # Build the documentation
firefox html/index.html # Alternative: `google-chrome html/index.html` etc.
```

## Working on the project
1. `catkin build` will only rebuild a binary if the source code changes so this command will only take a few minutes in the future. Since building the whole project from scratch takes a *long* time, be careful about cleaning the workspace. Avoid using `catkin clean` if possible. Rather use `catkin clean my_package` if one of the packages produces build errors.
1. Always source the *catkin* workspace in a new terminal: `source catkin_ws/devel/setup.bash`
1. Optional: Save the previous command to your `.bashrc` so it gets executed when opening a new terminal, i.e. `echo 'source <path_to_this_project>/catkin_ws/devel/setup.bash' >> ~/.bashrc`
